<?php

return [
    'date_format'=>'d/m/Y',
    'datetime_display_format'=>'d/m/Y h:i A',
    'date_display_format'=>'d/m/Y',
    'datetime_create_from_format'=>'d/m/Y h:i A',
    'https'=>env('APP_HTTPS',false),


    'app_https'=>env('APP_HTTPS',null),
    'saml_url'=>env('SAML_URL',null),
    'ohse_assessment_domain'=>env('OHSE_ASSESSMENT_DOMAIN',null),
    'facility_service_domain'=>env('FACILITY_SERVICE_DOMAIN',null),
    'marketing_merchandise_domain'=>env('MARKETING_MERCHANDISE_DOMAIN',null),
    'marketing_design_domain'=>env('MARKETING_DESIGN_DOMAIN',null),
    'media_domain'=>env('MEDIA_DOMAIN',null),
    'ems_domain'=>env('EMS_DOMAIN',null),
    'development'=>env('DEVELOPMENT', false),

    'venue_management_system_url'=>'http://intranet.monash.edu.au/tools/venue-bookings/?_ga=2.260940493.134449103.1529459309-2024851768.1520303604',
    'recreational_booking_system_url'=>'https://fmu-rbs.monash.edu.my',
];


