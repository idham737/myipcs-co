<?php

// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'census-operation', 'namespace' => 'CensusOperation', 'as' => 'census-operation.'], function () {
        Route::group(['prefix' => 'edoc1-management', 'namespace' => 'Edoc1Management', 'as' => 'edoc1-management.'], function () {

            Route::get('edoc1-verifications', 'Edoc1VerificationController@index')->name('edoc1-verifications.index');
            Route::get('edoc1-verifications/{edoc1_verification}', 'Edoc1VerificationController@show')->name('edoc1-verifications.show');
            Route::get('edoc1-verifications/{edoc1_verification}/detail', 'Edoc1VerificationController@detail')->name('edoc1-verifications.detail');
            Route::get('edoc2-verifications/{edoc2_verification}', 'Edoc2VerificationController@index')->name('edoc2-verifications.index');

        });
    });


    Route::resource('dashboard', 'DashboardController');
    Route::get('/', 'DashboardController@index')->name('dashboard');

});

\Auth::routes();

Route::any('logout', 'Auth\LoginController@logout');


Route::get('media/{id}', function ($id) {

    $media = \Spatie\MediaLibrary\Models\Media::find($id);

    if (!$media) {
        return response()->file(public_path('images/no-product-image.png'));
    }

    if ($conversion = request()->input('conversion', null)) {
        return response()->file($media->getPath($conversion));

    }

    return response()->file($media->getPath());

})->name('media.show');

Route::get('/personal', function () {
    return view('Co_Management.cati_officer.personal_info');
});

Route::get('/personal2', function () {
    return view('Co_Management.cati_officer.personal_info2');
})->name('personal2');

Route::get('create-chart/{type}','ChartController@makeChart');

Route::get('/assignjob', function () {
    return view('Co_Management.job_assignment.assign_job');
});

Route::get('/total_household', function () {
    return view('eRKL.total_household');
});

Route::get('/summary_household_state', function () {
    return view('eRKL.summary_household_state');
});

Route::get('/edoc24', function () {
    return view('eRKL.edoc24');
});

Route::get('householdST', 'HouseholdController@index')->name('householdST');
Route::get('householdAD', 'HouseholdController@householdAD')->name('householdAD');
Route::get('householdCD', 'HouseholdController@householdCD')->name('householdCD');
Route::get('householdCC', 'HouseholdController@householdCC')->name('householdCC');
Route::get('householdEB', 'HouseholdController@householdEB')->name('householdEB');

Route::get('genderST', 'GenderController@index')->name('genderST');
Route::get('genderEB', 'GenderController@genderEB')->name('genderEB');
Route::get('genderCC', 'GenderController@genderCC')->name('genderCC');
Route::get('genderCD', 'GenderController@genderCD')->name('genderCD');
Route::get('genderAD', 'GenderController@genderAD')->name('genderAD');

Route::get('worklist_edoc24', 'WorklistController@index')->name('worklist_edoc24');
Route::post('worklist_update', 'WorklistController@update')->name('worklist_update');
Route::get('edoc24', 'WorklistController@edoc24')->name('edoc24');
Route::get('/getPDF','WorklistController@getPDF')->name('getPDF');
Route::get('dynamic_pdf','DynamicPDFController@index');
Route::get('dynamic_pdf/pdf','DynamicPDFController@pdf');

Route::get('barchart_compare', 'HouseholdController@barchartCompare')->name('barchart_compare');

Route::get('/citizenship_state', function () {
    return view('eRKL.citizenship_state');
});

Route::get('/citizenship_ad', function () {
    return view('eRKL.citizenship_ad');
});

Route::get('/citizenship_cd', function () {
    return view('eRKL.citizenship_cd');
});

Route::get('/citizenship_cc', function () {
    return view('eRKL.citizenship_cc');
});

Route::get('/citizenship_eb', function () {
    return view('eRKL.citizenship_eb');
});

Route::get('/etnicity_state', function () {
    return view('eRKL.etnicity_state');
});

Route::get('/etnicity_ad', function () {
    return view('eRKL.etnicity_ad');
});

Route::get('/etnicity_cd', function () {
    return view('eRKL.etnicity_cd');
});

Route::get('/etnicity_cc', function () {
    return view('eRKL.etnicity_cc');
});

Route::get('/etnicity_eb', function () {
    return view('eRKL.etnicity_eb');
});