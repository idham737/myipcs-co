let mix = require('laravel-mix');

mix.js('resources/assets/js/bootstrap.js', 'public/js')
  .js('resources/assets/js/main.js', 'public/js')
  .js('resources/assets/js/product-preview.js', 'public/js')
  .js('resources/assets/js/product-inquiry.js', 'public/js')
  .js('resources/assets/js/products.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css/app.css');
mix.copy('resources/assets/images/logo.png', 'public/images');
// mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/css/webfonts');
