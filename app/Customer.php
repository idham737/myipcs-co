<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['id','firstname','lastname','sex','email','phone'];
    protected $table = 'customer';
}
