<?php namespace App;


class EnumerationBlock extends Base {

    protected $fillable=[
        'census_circle_id',
        'slug',
        'code',
        'number_code',
        'name',
        'description',
        'is_active',
    ];

    public function censusCircle(){
        return $this->belongsTo(CensusCircle::class,'census_circle_id');
    }
    public function livingQuarters(){
        return $this->hasMany(LivingQuarter::class,'enumeration_block_id');
    }

    public function households(){
        return $this->hasManyThrough(Household::class,LivingQuarter::class,'enumeration_block_id','living_quarter_id');
    }

}
