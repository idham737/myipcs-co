<?php namespace App;

use App\Traits\HasUuid;
use App\Traits\ModelUtility;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Base extends Model
{
    use SoftDeletes;
    use ModelUtility;


    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function statusHistory()
    {
        return $this->hasOne(StatusHistory::class, 'model_id')->whereModelType(static::class)->latest('status_histories.created_at');
    }
    public function statusHistories()
    {
        return $this->hasMany(StatusHistory::class, 'model_id')->whereModelType(static::class);
    }


    public function isStatus($statusCode){

        if(is_array($statusCode)){
            return in_array(@$this->status->code,$statusCode);
        }

        return @$this->status->code===$statusCode;
    }

    public function findByUuid($uuid){
        return static::where('uuid',$uuid)->first();
    }

    public function findByHashid($hashid){
        return static::findOrFail(\Hashids::decode($hashid)[0]);
    }

    public function getHashid($columnName='id'){
        return \Hashids::encode($this->{$columnName});
    }

    public function getValidationRules(){
        return [];
    }
    public function getValidationMessages(){
        return [];
    }

    public function enable(){
        $this->update(['is_active'=>1]);
        return $this;
    }
    public function disable(){
        $this->update(['is_active'=>0]);
        return $this;
    }

    public function scopeActive($q){
        return $q->where('is_active',1);
    }

    public function changeStatus($status, $remark = null)
    {

        if (!$status) {
            throw new \Exception('status not valid');
        }

        if (!$status instanceof Status) {
            $status = Status::whereCode($status)->firstOrFail();
        }

        $previousStatusId = $this->status_id;

        $this->update([
            'status_id' => $status->id,
        ]);

        $user = auth()->user();


        $this->statusHistory()->create([
            'status_id' => $status->id,
            'user_id' => $user->id ?? null,
            'model_type' => static::class,
            'remark' => $remark,
            'previous_status_id' => $previousStatusId,
            'previous_status_history_id' => $this->statusHistory->id??null,
        ]);


        $this->refresh();


        return $this;
    }

}
