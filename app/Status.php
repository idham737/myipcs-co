<?php namespace App;

class Status extends Base
{
    protected $fillable=[
        'slug',
        'code',
        'name',
        'name2',
        'description',
        'is_active',
        'class',
        'color',
        'short_name',
        'icon',
    ];

}
