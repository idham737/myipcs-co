<?php namespace App;

class StatusHistory extends Base
{
    protected $fillable=[
        'model_id',
        'status_id',
        'previous_status_id',
        'previous_status_history_id',

        'user_id',
        'model_type',
        'remark',
        'group',
    ];


    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function status(){
        return $this->belongsTo(Status::class,'status_id');
    }
    public function previousStatus(){
        return $this->belongsTo(Status::class,'previous_status_id');
    }
    public function previousStatusHistory(){
        return $this->belongsTo(StatusHistory::class,'previous_status_history_id');
    }
}
