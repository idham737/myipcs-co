<?php namespace App\Traits;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Ramsey\Uuid\Uuid;

trait ModelUtility
{
    public static function bootModelUtility()
    {

    }

    public static function findByUuid($uuid){
        return static::where('uuid',$uuid)->first();
    }

    public function scopeUuidOrId($q,$uuidOrId){
        return $q->where(function($q)use($uuidOrId){
            return $q->where('uuid',$uuidOrId)->orWhere('id',$uuidOrId);
        });
    }

    public function creator(){
        return $this->belongsTo(User::class,'creator_id');
    }

    public static function findByCreatorId($id){
        return static::where('creator_id',$id)->first();
    }

    public function scopeCreatorId($q,$id){
        return $q->where('creator_id',$id);
    }

}
