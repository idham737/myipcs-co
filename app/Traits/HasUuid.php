<?php namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Ramsey\Uuid\Uuid;

trait HasUuid
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function bootHasUuid()
    {
        self::creating(function ($model) {

            if(!$model->getAttribute('uuid')){
                $model->setAttribute('uuid', Uuid::uuid4());
            }


        });
    }

    /**
     * Scope query using unique Id
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string $uuid
     * @return \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeUuid(Builder $query, $uuid)
    {
        return $query->where('uuid', $uuid);
    }
}
