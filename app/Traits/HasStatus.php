<?php namespace App\Traits;

use App\Status;
use App\StatusHistory;

trait HasStatus
{
    public static function bootHasStatus()
    {

    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }
    public function statusHistory()
    {
        return $this->hasOne(StatusHistory::class, 'model_id')->whereModelType(static::class)->latest('status_histories.created_at');
    }
    public function statusHistories()
    {
        return $this->hasMany(StatusHistory::class, 'model_id')->whereModelType(static::class);
    }

    public function isStatus($statusCode){

        if(is_array($statusCode)){
            return in_array($this->status->code??null,$statusCode);
        }

        return $this->status->code??null===$statusCode;
    }

    public function changeStatus($status, $remark = null)
    {

        if (!$status) {
            throw new \Exception('status not valid');
        }

        if (!$status instanceof Status) {
            $status = Status::whereCode($status)->firstOrFail();
        }

        $previousStatusId = $this->status_id;

        $this->update([
            'status_id' => $status->id,
        ]);

        $user = auth()->user();


        $this->statusHistory()->create([
            'status_id' => $status->id,
            'user_id' => $user->id ?? null,
            'model_type' => static::class,
            'remark' => $remark,
            'previous_status_id' => $previousStatusId,
            'previous_status_history_id' => $this->statusHistory->id??null,
        ]);


        $this->refresh();


        return $this;
    }
}
