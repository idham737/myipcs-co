<?php namespace App;

use Spatie\Activitylog\Models\Activity;

class ActivityLog extends Activity
{
    protected $fillable=[
        'log_name',
        'description',
        'subject_id',
        'subject_type',
        'causer_id',
        'causer_type',
        'properties',
    ];

}
