<?php namespace App;


class AdministrativeDistrict extends Base {

    protected $fillable=[
        'state_id',
        'slug',
        'code',
        'number_code',
        'name',
        'description',
        'is_active',
    ];

    public function state(){
        return $this->belongsTo(State::class,'state_id');
    }
}
