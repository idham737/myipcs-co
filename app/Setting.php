<?php namespace App;

class Setting extends Base
{
    protected $fillable=[
        'key',
        'label',
        'value',
    ];



    public static function getValueByKey($key){
        return Setting::where('key',$key)->first()->value??null;
    }

}
