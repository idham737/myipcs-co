<?php namespace App\Services;

use Collective\Html\FormBuilder;

class Macros extends FormBuilder {


    public function error($name,$messageBagName='')
    {
        $errors=\Session::get('errors');

        if(!$errors){
            return;
        }

        $splitName=explode('.',$name);

        $newName='';
        foreach ($splitName as $index=>$item) {
            if($index==0){
                $newName.=$item;
            }else{
                $newName.='[\''.$item.'\']';
            }
        }

        if($messageBagName===''){
            $errorHas=$errors->has($name);
            $errorString=$errors->first($name);
        }else{
            $errorHas=$errors->{$messageBagName}->has($name);
            $errorString=$errors->{$messageBagName}->first($name);

        }

        return $errorHas?'<span class="invalid-feedback" style="display:block !important;" data-input-name="'.$newName.'" data-message-bag-name="'.$messageBagName.'">'.$errorString.'</span>':'';
    }
}
