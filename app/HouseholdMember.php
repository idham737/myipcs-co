<?php namespace App;


class HouseholdMember extends Base {

    protected $fillable=[
        'household_id',
        'household_member_information',
        'is_active',
        'status_id',
    ];

    protected $casts=[
        'household_member_information'=>'array',
    ];

    public function household(){
        return $this->belongsTo(Household::class,'household_id');
    }
}
