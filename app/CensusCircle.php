<?php namespace App;


class CensusCircle extends Base {

    protected $fillable=[
        'census_district_id',
        'slug',
        'code',
        'number_code',
        'name',
        'description',
        'is_active',
    ];

    public function censusDistrict(){
        return $this->belongsTo(CensusDistrict::class,'census_district_id');
    }
}
