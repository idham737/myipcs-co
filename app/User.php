<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Base implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use Notifiable,HasRoles;
    use SoftDeletes;
    use LogsActivity;

    protected $guard_name = 'web';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'phone_number',
        'email',
        'superior_id',
        'verified',
        'is_active',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getValidationRules($override=[]){
        return array_merge([
            'email'=>'required|exists:users,email',
            'password'=>'required',
            'name'=>'required',
        ],$override);
    }
    public function getValidationMessages($override=[]){
        return array_merge([
        ],$override);
    }


}
