<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class BaseResource extends Resource
{

    protected $hiddenAttributes = [];


    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function hidden($hiddenAttributes = [])
    {
        $this->hiddenAttributes = $hiddenAttributes;
        return $this;
    }

    public function processModelAndTransform($columns = [], $relations = [], $additional = [])
    {

        $model = $this;

        $data = [];
        $hidden = $this->getHidden();
        $hidden = array_merge($hidden, $this->hiddenAttributes);
        $columns = array_diff($columns, $hidden);
        foreach ($columns as $column) {
            $data[$column] = $model->{$column};
        }


        $data = array_merge([
            'id' => $model->id,
        ], $data, $relations, $additional);
        $data = array_merge($data, [
            'created_at' => $model->created_at ? $model->created_at->toRfc3339String() : null,
            'updated_at' => $model->updated_at ? $model->updated_at->toRfc3339String() : null,
            'deleted_at' => $model->deleted_at ? $model->deleted_at->toRfc3339String() : null,
        ]);


        return $data;

    }

    public function item($relation, $resourceClassName)
    {
        return $resourceClassName::make($this->whenLoaded($relation));
    }

    public function items($relation, $resourceClassName)
    {
        return $resourceClassName::collection($this->whenLoaded($relation));

    }

}
