<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory as ViewFactory;

class BaseController extends Controller
{
    protected $model;
    protected $baseRoute;
    protected $baseView ;
    protected $resourceName ;
    protected $pageHeaderText ;


    public function __construct()
    {
        view()->share([
            'baseRoute' => $this->baseRoute,
            'baseView' => $this->baseView,
            'resourceName' => $this->resourceName,
            'pageHeaderText'=>$this->pageHeaderText,
        ]);
    }

    public function authUser(){
        return auth()->user();
    }

    public function authUserId(){
        return $this->authUser()->id;
    }

    protected function authorizedRole($role){
        if(!is_array($role)){
            $role=[$role];
        }

        $authUser=auth()->user();

        if(!$authUser->hasRole($role)){
            return false;
        }

        return true;
    }

    public function page403(){
        return response()->view('errors.403', [], 403);
    }
    public function page404(){
        return response()->view('errors.404', [], 404);
    }



    protected function view($view = null, $data = [], $mergeData = [])
    {


        $factory = app(ViewFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        if(is_array($view)){
            $view=implode('.',$view);
        }

        return $factory->make($view, $data, $mergeData);
    }

    public function redirectRoute($route=null,$data=[]){

        if(is_array($route)){
            $route=implode('.',$route);
        }

        return redirect()->route($route,$data);
    }

    protected function jsValidate($rules=[],$messages=[]){
        $jsValidator=\JsValidator::make($rules,$messages);
        view()->share(['jsValidator'=>$jsValidator]);
        return $jsValidator;
    }

    protected function jsValidates($items=[]){

        $jsValidators=[];
        foreach($items as $formSelector=>$item){
            $jsValidators[]=[
                'selector'=>$item[0],
                'validator'=>\JsValidator::make($item[1]??[],$item[2]??[]),
            ];
        }

        view()->share(['jsValidators'=>$jsValidators]);

        return $jsValidators;
    }

    protected function alertCreated($text,$title='Created',$type='info'){
        return alert()->{$type}($title,$text);
    }
    protected function alertInfo($text,$title='Attention',$type='info'){
        return alert()->{$type}($title,$text);
    }
    protected function alertWarning($text,$title='Warning',$type='warning'){
        return alert()->{$type}($title,$text);
    }
    protected function alertUpdated($text,$title='Updated',$type='info'){
        return alert()->{$type}($title,$text);
    }
    protected function alertDeleted($text,$title='Deleted',$type='warning'){
        return alert()->{$type}($title,$text);
    }
    protected function alertError($text,$title='Error',$type='error'){
        return alert()->{$type}($title,$text);
    }
    protected function toastError($text,$type='error'){
        return alert()->toast($text,$type);
    }
}
