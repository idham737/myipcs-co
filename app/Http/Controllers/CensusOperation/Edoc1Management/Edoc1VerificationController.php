<?php namespace App\Http\Controllers\CensusOperation\Edoc1Management;


use App\EnumerationBlock;
use App\Household;
use App\HouseholdMember;
use App\LivingQuarter;
use App\Setting;

class Edoc1VerificationController extends BaseController
{
    protected $baseRoute = 'census-operation.edoc1-management.edoc1-verifications';
    protected $baseView = 'census_operation.edoc1_management.edoc1_verification';
//    protected $pageHeaderText = 'Pen';
//    protected $resourceName = 'Credit Package';

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $models=EnumerationBlock::get();
        return $this->view([$this->baseView, 'index'], compact('models'));
    }

    public function show($id)
    {
        $enumerationBlock=EnumerationBlock::find($id);
        $livingQuarters=$enumerationBlock->livingQuarters;
        $households=$enumerationBlock->households;
        $householdMembers=HouseholdMember::whereIn('household_id',$households->pluck('id')->toArray())->get();
        return $this->view([$this->baseView, 'show'], compact('enumerationBlock','households','livingQuarters','householdMembers'));
    }

    public function detail($id)
    {
        $household=Household::find($id);
        return $this->view([$this->baseView, 'detail'], compact('household'));
    }

}
