<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\WorklistEdoc24;
use App\Customer;
use DB;
use Illuminate\Support\Facades\Input;
use PDF;

class WorklistController extends Controller
{
    public function index(){

        $worklist_edoc24= WorklistEdoc24::all();
        // dd($gender_st);
        return view('eRKL.worklist_edoc24',compact('worklist_edoc24'));
    }

    public function edoc24(){

        $customers=DB::table('customer as A')
                ->select('A.*')
                ->orderBy('A.id','ASC')->get();
        // $worklist_edoc24= WorklistEdoc24::all();
        // dd($gender_st);
        return view('eRKL.edoc24',compact('customers'));
    }

    public function update(Request $request){
        $updates=$request->update;
    
                if($updates){
                    foreach($updates as $upda){
                        DB::table('worklist_edoc24')
                        ->where('id',$upda)
                        ->update(['approval_status' => 2]);
                    }
                    return back();
                }else{
                    return back();
                }
        //dd('$updates');
        // $upda=$request->worklist_update;
        // print_r($upda);
    }

    public function getPDF()
    {
        $customers=DB::table('customer as A')
                ->select('A.*')
                ->orderBy('A.id','ASC')->get();
	    $pdf=PDF::loadView('eRKL.edoc24',['customers'=>$customers]);
        return $pdf->stream('edoc24.pdf',compact('customers'));
        // dd($customers);
    }
}
