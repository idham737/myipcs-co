<?php namespace App\Http\Controllers;


class DashboardController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('dashboard.index', compact(''));
    }
}
