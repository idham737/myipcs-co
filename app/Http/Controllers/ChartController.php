<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Charts;

use App\User;

use DB;

use App\http\Requests;



class ChartController extends Controller

{

    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function makeChart($type)

    {

        switch ($type) {

            case 'bar':

                $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::database($users, 'bar', 'highcharts') 

                            ->title("Senarai User Mengikut Blok Penghitungan") 

                            ->elementLabel("Jumlah User")
                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('bp_name');

                            // ->groupByMonth(date('Y'), true);

                break;



            case 'pie':

                $chart = Charts::create('pie', 'highcharts')

                            ->title('HDTuto.com Laravel Pie Chart')

                            ->labels(['Codeigniter', 'Laravel', 'PHP'])

                            ->values([5,10,20])

                            ->dimensions(1000,500)

                            ->responsive(true);

                break;



            case 'donut':

                $chart = Charts::create('donut', 'highcharts')

                            ->title('HDTuto.com Laravel Donut Chart')

                            ->labels(['First', 'Second', 'Third'])

                            ->values([5,10,20])

                            ->dimensions(1000,500)

                            ->responsive(true);

                break;



            case 'line':

                $chart = Charts::create('line', 'highcharts')

                            ->title('HDTuto.com Laravel Line Chart')

                            ->elementLabel('HDTuto.com Laravel Line Chart Lable')

                            ->labels(['First', 'Second', 'Third'])

                            ->values([5,10,20])

                            ->dimensions(1000,500)

                            ->responsive(true);

                break;



            case 'area':

                $chart = Charts::create('area', 'highcharts')

                            ->title('HDTuto.com Laravel Area Chart')

                            ->elementLabel('HDTuto.com Laravel Line Chart label')

                            ->labels(['First', 'Second', 'Third'])

                            ->values([5,10,20])

                            ->dimensions(1000,500)

                            ->responsive(true);

                break;



            case 'geo':

                $chart = Charts::create('geo', 'highcharts')

                            ->title('HDTuto.com Laravel GEO Chart')

                            ->elementLabel('HDTuto.com Laravel GEO Chart label')

                            ->labels(['ES', 'FR', 'RU'])

                            ->colors(['#3D3D3D', '#985689'])

                            ->values([5,10,20])

                            ->dimensions(1000,500)

                            ->responsive(true);

                break;



            default:

                # code...

                break;

        }

        return view('Co_Management.cati_officer.chart', compact('chart'));

    }

}