<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HouseholdState;
use App\HouseholdAD;
use App\HouseholdCD;
use App\HouseholdCC;
use App\HouseholdEB;
use App\User;
use Charts;
use DB;
use App\http\Requests;

class HouseholdController extends Controller
{
    public function index(){

        $household_st= HouseholdState::all();
        // dd($gender_st);
        return view('eRKL.summary_household_state',compact('household_st'));
    }

    public function householdAD(){

        $household_ad = HouseholdAD::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::database($household_ad, 'bar', 'highcharts') 

                            ->title("Daerah Pentadbiran") 

                            ->elementLabel("Isi Rumah")
                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('ad_id');

                            // ->groupByMonth(date('Y'), true);
                            // dd($household_ad);

        return view('eRKL.summary_household_ad',compact('chart'));
    }

    public function householdCD(){

        $household_cd = HouseholdCD::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::database($household_cd, 'bar', 'highcharts') 

                            ->title("Daerah Banci") 

                            ->elementLabel("Isi Rumah")
                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('cd_id');

                            // ->groupByMonth(date('Y'), true);
                            // dd($household_ad);

        return view('eRKL.summary_household_cd',compact('chart'));
    }

    public function householdCC(){

        $household_cc = HouseholdCC::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::database($household_cc, 'bar', 'highcharts') 

                            ->title("Lingkungan Banci") 

                            ->elementLabel("Isi Rumah")
                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('cc_id');

                            // ->groupByMonth(date('Y'), true);
                            // dd($household_ad);

        return view('eRKL.summary_household_cc',compact('chart'));
    }

    public function householdEB(){

        $household_eb = HouseholdEB::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::database($household_eb, 'bar', 'highcharts') 

                            ->title("Blok Penghitungan") 

                            ->elementLabel("Isi Rumah")
                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('eb_id');

                            // ->groupByMonth(date('Y'), true);
                            // dd($household_ad);

        return view('eRKL.summary_household_eb',compact('chart'));
    }

    public function barchartCompare(){

        // $household_eb = HouseholdEB::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y')) 

        //                 ->get();

        // $household_eb2 = HouseholdEB::where(DB::raw("(DATE_FORMAT(updated_at,'%Y'))"),date('Y')) 

        //                 ->get();

        $bp1a = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 1')
                        ->where('status', '=', '0') 
                        ->get()
                        ->count();
        $bp1b = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 1')
                        ->where('status', '=', '1') 
                        ->get()
                        ->count();
        $bp2a = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 2')
                        ->where('status', '=', '0') 
                        ->get()
                        ->count();
        $bp2b = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 2')
                        ->where('status', '=', '1') 
                        ->get()
                        ->count();
        $bp3 = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 3') 
                        ->get();
        $bp4 = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 4') 
                        ->get();
        $bp5 = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 5') 
                        ->get();
        $bp6 = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 6') 
                        ->get();
        $bp7 = DB::table('household_eb')
                        ->select('household_eb.eb_id') 
                        ->where('eb_id', '=', 'BP 7') 
                        ->get();

                // $chart = Charts::multiDatabase('line', 'material')
                // ->dataset('Element 1', User::all());
                // ->dataset('Element 2', Posts::all());

                $chart = Charts::multidatabase('bar', 'highcharts') 

                            ->title("Blok Penghitungan") 

                            ->elementLabel("Isi Rumah")

                            ->dataset('Selesai',[$bp1b,$bp2b])
                            ->dataset('Tidak Selesai',[$bp1a,$bp2a])
                            // ->dataset('BP 2',$bp2)
                            // ->dataset('BP 3',$bp3)
                            // ->dataset('BP 4',$bp4)
                            // ->dataset('BP 5',$bp5)
                            // ->dataset('BP 6',$bp6)
                            // ->dataset('BP 7',$bp7)

                            
                            // ->labels(["1","2","3","4","5","6","7","8","9","10","11","12"])

                            // ->dimensions(1000, 500) 

                            ->colors(['#0099cc','#808080','#66ff66','#ff9933'])

                            ->responsive(true)
                            
                            ->groupBy('eb_id');

                            // ->groupByMonth(date('Y'), true);
                            // dd($household_ad);

        return view('eRKL.summary_household_eb',compact('chart'));
    }
}
