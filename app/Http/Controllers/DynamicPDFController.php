<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class DynamicPDFController extends Controller
{
    function index(){
        $customer_data = $this->get_customer_data();
        return view('dynamic_pdf')->with('customer_data',$customer_data);
    }

    function get_customer_data(){
        $customer_data = DB::table('customer')
                        ->limit(3)
                        ->get();
        return $customer_data;
    }

    function pdf(){
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_customer_data_to_html());
        $pdf->stream();
    }

    function convert_customer_data_to_html(){
        $customer_data = $this->get_customer_data();
        $output = '
        <h3 align="center">Customer Data</h3>
        <table width="100%" style="border-collapse:collapse; border: 0px;">
            <tr>
                <th style="border: 1px solid; padding:12px;" width="">ID</th>
                <th style="border: 1px solid; padding:12px;" width="">First Name</th>
                <th style="border: 1px solid; padding:12px;" width="">Last Name</th>
                <th style="border: 1px solid; padding:12px;" width="">Sex</th>
                <th style="border: 1px solid; padding:12px;" width="">Email</th>
                <th style="border: 1px solid; padding:12px;" width="">Phone</th>
            </tr>
        ';
        foreach($customer_data as $key => $c){
            $output .='
            <tr>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->id.'</td>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->firstname.'</td>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->lastname.'</td>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->sex.'</td>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->email.'</td>
                <td style="border: 1px solid; padding:12px;" width="">'.$c->phone.'</td>
            </tr>
            ';
        }
        $output .='</table>';
        return $output;
    }
}
