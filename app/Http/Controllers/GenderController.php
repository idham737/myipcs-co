<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\GenderState;
use App\GenderAD;
use App\GenderCD;
use App\GenderCC;
use App\GenderEB; 

class GenderController extends Controller
{
    public function index(){

        $gender_st= GenderState::all();
        // dd($gender_st);
        return view('eRKL.summary_gender_state',compact('gender_st'));
    }

    public function genderEB(){

        $gender_eb= GenderEB::all();
        // dd($gender_eb);
        return view('eRKL.summary_gender_eb',compact('gender_eb'));
    }

    public function genderCC(){

        $gender_cc= GenderCC::all();
        // dd($gender_eb);
        return view('eRKL.summary_gender_cc',compact('gender_cc'));
    }

    public function genderCD(){

        $gender_cd= GenderCD::all();
        // dd($gender_eb);
        return view('eRKL.summary_gender_cd',compact('gender_cd'));
    }

    public function genderAD(){

        $gender_ad= GenderAD::all();
        // dd($gender_eb);
        return view('eRKL.summary_gender_ad',compact('gender_ad'));
    }
}