<?php namespace App;

class EmailLog extends Base
{
    protected $fillable=[
        'template_id',
        'status_id',
        'to',
        'subject',
        'message',
    ];

}
