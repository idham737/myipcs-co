<?php
namespace App\Listeners;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use App\User;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Saml2Logon {
    public function __contruct()
    {

    }

    public function handle(Saml2LoginEvent $event)
    {
        Log::debug('Saml logon response.');

        $samlUser = $event->getSaml2User();
        $attributes = $samlUser->getAttributes();
//        dd($attributes);
//
//        Log::debug('Logged in username:' . $samlUser->getUserId());
//        Log::debug('Logged in user info:' . print_r($attributes, true));

        $username = $samlUser->getUserId();
        $name = $attributes['name'][0];
        $email = $attributes['email'][0];

//        if ( is_array($attributes) && count($attributes) ) {
//            foreach ( $attributes as $key => $values ) {
//                if ( preg_match('/\/givenname$/', $key) ) {
//                    $name = array_first($values);
//                } else if ( preg_match('/\/emailaddress$/', $key) ) {
//                    $email = array_first($values);
//                    $email = strtolower($email);
//                }
//
//                if ( $name && $email ) {
//                    break ;
//                }
//            }
//        }

        $user = User::where('username',$username)->first();

        if(!$user){
            //check email is student or not
            if(str_contains($email,'@student.monash')){
                throw new HttpException(403,'Student account, auto creation not possible');
            }else{
                $user=User::create([
                    'username'=>$username,
                    'email'=>$email,
                    'name'=>$name,
                ]);
                $user->assignRole('requester');
            }
        }

        if($user){
            $user->update([
                'email'=>$email,
                'name'=>$name,
            ]);
        }

        Auth::login($user);
    }
}
