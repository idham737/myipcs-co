<?php namespace App;


class LivingQuarter extends Base {

    protected $fillable=[
        'administrative_district_id',
        'census_district_id',
        'census_circle_id',
        'enumeration_block_id',
        'status_id',
        'living_quarter_information',
        'mukim',
        'strata',
        'is_active',
    ];

    protected $casts=[
        'living_quarter_information'=>'array',
    ];


    public function administrativeDistrict(){
        return $this->belongsTo(AdministrativeDistrict::class,'administrative_district_id');
    }
    public function censusDistrict(){
        return $this->belongsTo(CensusDistrict::class,'census_district_id');
    }
    public function censusCircle(){
        return $this->belongsTo(CensusCircle::class,'census_circle_id');
    }
    public function enumerationBlock(){
        return $this->belongsTo(EnumerationBlock::class,'enumeration_block_id');
    }
    public function households(){
        return $this->hasMany(Household::class,'living_quarter_id');
    }
}
