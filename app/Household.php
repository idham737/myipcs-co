<?php namespace App;


class Household extends Base {

    protected $fillable=[
        'living_quarter_id',
        'household_head_id',
        'status_id',
        'household_information',
        'is_active',
    ];

    protected $casts=[
        'household_information'=>'array',
    ];

    public function livingQuarter(){
        return $this->belongsTo(LivingQuarter::class,'living_quarter_id');
    }
    public function householdHead(){
        return $this->belongsTo(HouseholdMember::class,'household_head_id');
    }

    public function householdMembers(){
        return $this->hasMany(HouseholdMember::class,'household_id');
    }
}
