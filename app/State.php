<?php namespace App;


class State extends Base {

    protected $fillable=[
        'country_id',
        'slug',
        'code',
        'number_code',
        'name',
        'description',
        'is_active',
    ];

    public function country(){
        return $this->belongsTo(Country::class,'country_id');
    }
}
