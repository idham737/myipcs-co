<?php

if (! function_exists('get_group')) {
    function get_group()
    {
        $group = null;
        switch (request()->getHttpHost()) {
            case config('core.ohse_assessment_domain'):
                $group = 'ohseAssessment';
                break;
            case config('core.facility_service_domain'):
                $group = 'facilityService';
                break;
            case config('core.marketing_merchandise_domain'):
                $group = 'marketingMerchandise';
                break;
            case config('core.marketing_design_domain'):
                $group = 'marketingDesign';
                break;
            case config('core.media_domain'):
                $group = 'media';
                break;
            case config('core.ems_domain'):
                $group = 'ems';
                break;
        }
        return $group;
    }
}

if (! function_exists('isGroup')) {
    function isGroup($groupName=null)
    {
        if(is_string($groupName)){
            return get_group()==$groupName;
        }

        if(is_array($groupName)){
            return in_array(get_group(),$groupName);
        }

        return false;

    }
}

if (! function_exists('authUserIs')) {
    function authUserIs($roles=null)
    {
        return auth()->user()->hasRole($roles);
    }
}
if (! function_exists('authUserIsNot')) {
    function authUserIsNot($roles=null)
    {
        return !auth()->user()->hasRole($roles);
    }
}
