<?php namespace App;


class CensusDistrict extends Base {

    protected $fillable=[
        'administrative_district_id',
        'slug',
        'code',
        'number_code',
        'name',
        'description',
        'is_active',
    ];

    public function administrativeDistrict(){
        return $this->belongsTo(AdministrativeDistrict::class,'administrative_district_id');
    }
}
