<?php namespace App;


class Country extends Base {

    protected $fillable=[
        'slug',
        'code',
        'short_code',
        'name',
        'description',
        'is_active',
    ];
}
