<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseholdMembersTable extends Migration
{
    protected $table='household_members';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('household_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();

            $table->longText('household_member_information')->nullable();
            $table->integer('is_active')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
