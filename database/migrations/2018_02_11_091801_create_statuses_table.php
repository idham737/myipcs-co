<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration
{
    protected $table='statuses';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->nullable();
            $table->string('code')->nullable();
            $table->string('name')->nullable();
            $table->string('name2')->nullable();
            $table->string('class')->nullable();
            $table->string('color')->nullable();
            $table->text('description')->nullable();
            $table->integer('is_active')->nullable();
            $table->string('short_name')->nullable();
            $table->string('icon')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
