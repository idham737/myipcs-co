<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    protected $table='countries';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {

            $table->increments('id');

            $table->string('slug')->nullable();
            $table->string('code')->nullable();
            $table->string('short_code')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('is_active')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
