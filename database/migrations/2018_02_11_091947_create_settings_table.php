<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    protected $table='settings';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');


            $table->string('key')->nullable();
            $table->string('label')->nullable();
            $table->text('value')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
