<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivingQuartersTable extends Migration
{
    protected $table='living_quarters';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('administrative_district_id')->nullable();
            $table->unsignedInteger('census_district_id')->nullable();
            $table->unsignedInteger('census_circle_id')->nullable();
            $table->unsignedInteger('enumeration_block_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();

            $table->longText('living_quarter_information')->nullable();
            $table->string('mukim')->nullable();
            $table->string('strata')->nullable();
            $table->integer('is_active')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
