<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHouseholdsTable extends Migration
{
    protected $table='households';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('living_quarter_id')->nullable();
            $table->unsignedInteger('household_head_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();

            $table->longText('household_information')->nullable();
            $table->integer('is_active')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
