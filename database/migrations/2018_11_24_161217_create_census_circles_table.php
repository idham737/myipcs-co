<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCensusCirclesTable extends Migration
{
    protected $table='census_circles';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {

            $table->increments('id');

            $table->unsignedInteger('census_district_id')->nullable();

            $table->string('slug')->nullable();
            $table->string('code')->nullable();
            $table->string('number_code')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->integer('is_active')->default(1)->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
