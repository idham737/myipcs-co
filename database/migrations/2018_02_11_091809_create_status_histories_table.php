<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusHistoriesTable extends Migration
{
    protected $table='status_histories';

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('model_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();
            $table->unsignedInteger('previous_status_id')->nullable();
            $table->unsignedInteger('previous_status_history_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();

            $table->string('model_type')->nullable();
            $table->string('model_group')->nullable();
            $table->text('remark')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists($this->table);
    }
}
