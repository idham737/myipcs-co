<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses=[
            ['name'=>'pending verification','name2'=>'menunggu pengesahan','class'=>'warning'],
            ['name'=>'verified','name2'=>'disahkan','class'=>'success'],
            ['name'=>'unverified','name2'=>'tidak sah','class'=>'danger'],
            ['name'=>'pending revisit','name2'=>'menunggu lawatan semula','class'=>'warning'],
            ['name'=>'revisited','name2'=>'dilawat semula','class'=>'info'],

        ];


        foreach($statuses as $item){
            \App\Status::create([
                'slug' => str_slug(@$item['name']),
                'code' => str_slug(strtolower(@$item['name']),'_'),
                'name' => ucwords(@$item['name']),
                'name2' => ucwords(@$item['name2']),
                'class' => (@$item['class']),
                'short_name' => (@$item['short_name']),
                'icon' => (@$item['icon']),
                'description'=>'',
                'is_active'=>1,
            ]);
        }
    }
}
