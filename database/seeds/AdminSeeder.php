<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user=\App\User::create([
            'name'=>'admin',
            'username'=>'admin',
            'password'=>\Hash::make('password'),
            'email'=>'admin@admin.com',
            'is_active'=>1,
        ]);

        $user->assignRole('admin');
    }
}
