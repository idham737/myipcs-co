<?php

use Illuminate\Database\Seeder;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();


        for($i=0;$i<10;$i++){
            $cencusCircle=\App\CensusCircle::create([
                'number_code'=>str_pad('0',2,$i+1),
            ]);

            for($x=0;$x<rand(2,5);$x++){
                $enumerationBlock=\App\EnumerationBlock::create([
                    'census_circle_id'=>$cencusCircle->id,
                    'number_code'=>str_pad('0',3,$x+1),
                ]);

                for($z=0;$z<rand(5,10);$z++){




                    $livingQuarter=\App\LivingQuarter::create([
                        'census_circle_id'=>$cencusCircle->id,
                        'enumeration_block_id'=>$enumerationBlock->id,
                    ]);

                    $livingQuarter->changeStatus('pending_verification');


                    $livingQuarterInformation=[
                        'ub_number'=>'01',
                        'living_quarter_number'=>str_pad('0',3,$livingQuarter->id),
                        'address'=>'address',
                        'living_quarter_code'=>'living_quarter_code',
                        'is_occupied'=>'is_occupied',
                        'house_number'=>'house_number',
                        'building_name'=>'building_name',
                        'road_name'=>'road_name',
                        'housing_name'=>'housing_name',
                        'town'=>'town',
                        'postcode'=>'postcode',
                        'state'=>'state',
                        'living_quarter_type'=>'living_quarter_type',
                        'number_of_storey'=>'number_of_storey',
                        'total_number_of_floor'=>'total_number_of_floor',
                        'construction_material_of_outer_wall'=>'construction_material_of_outer_wall',
                        'occupied'=>'occupied',
                        'vacant'=>'vacant',
                        'number_of_room'=>'number_of_room',
                        'number_of_bedroom'=>'number_of_bedroom',
                        'ownership_status'=>'ownership_status',
                        'drinking_water_supply_facility'=>'drinking_water_supply_facility',
                        'electricity_supply_facility'=>'electricity_supply_facility',
                        'toilet_facility'=>'toilet_facility',
                        'garbage_collection_facility'=>'garbage_collection_facility',
                    ];


                    $livingQuarter->update([
                        'living_quarter_information'=>$livingQuarterInformation,

                    ]);


                    for($v=0;$v<rand(1,3);$v++){

                        $household=\App\Household::create([
                            'living_quarter_id'=>$livingQuarter->id,
                        ]);
                        $household->changeStatus('pending_verification');

                        $householdInformation=[
                            'ub_number'=>'ub_number',
                            'monthly_rental_payment'=>'monthly_rental_payment',
                            'gross_monthly_income_received_from_all_source'=>'gross_monthly_income_received_from_all_source',
                            'motor_car'=>'motor_car',
                            'motorcycle'=>'motorcycle',
                            'bicycle'=>'bicycle',
                            'other_items'=>'other_items',
                            'anyone_in_this_household_own_this_living_quarter'=>'anyone_in_this_household_own_this_living_quarter',
                            'anyone_in_this_household_own_other_living_quarter'=>'anyone_in_this_household_own_other_living_quarter',
                            'is_this_household_paying_rent_for_this_living_quarter'=>'is_this_household_paying_rent_for_this_living_quarter',
                            'income_from_agro_based_activities_yes_no'=>'income_from_agro_based_activities_yes_no',
                            'income_from_agro_based_activities'=>'income_from_agro_based_activities',

                        ];


                        $household->update([
                            'household_information'=>$householdInformation,

                        ]);

                        for($b=0;$b<rand(1,5);$b++){

                            $householdMember=\App\HouseholdMember::create([
                                'household_id'=>$household->id,
                            ]);
                            $householdMember->changeStatus('pending_verification');

                            $householdMemberInformation=[
                                'ub_number'=>'ub_number',
                                'name'=>$faker->name,
                                'relation_to_head_of_household'=>'relation_to_head_of_household',
                                'sex'=>'sex',
                                'date_of_birth'=>'date_of_birth',
                                'age'=>'age',
                                'marital_status'=>'marital_status',
                                'ethnic_group_code'=>'ethnic_group_code',
                                'ethnic_group'=>'ethnic_group',
                                'religion'=>'religion',
                                'person_number'=>'person_number',
                                'birthplace_country'=>'birthplace_country',
                                'birthplace_state'=>'birthplace_state',

                            ];

                            $householdMember->update([
                                'household_member_information'=>$householdMemberInformation,

                            ]);
                        }



                        $household->update([
                            'household_head_id'=>$household->householdMembers->first()->id,

                        ]);

                    }
                }

            }
        }


    }

}
