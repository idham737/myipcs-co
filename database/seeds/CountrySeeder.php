<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items=$this->data();

        foreach($items as $item){
            \App\Country::updateOrCreate([
                'slug'=>str_slug($item['val2']),
            ],[
                'code'=>str_slug($item['val2'],'_'),
                'short_code'=>$item['val1'],
                'name'=>$item['val2'],
                'is_active'=>1,
            ]);
        }
    }

    protected function data(){
        return array(
            array("val0"=>1,"val1"=>"AF","val2"=>"Afganistan"),
            array("val0"=>2,"val1"=>"BH","val2"=>"Bahrain"),
            array("val0"=>3,"val1"=>"BD","val2"=>"Bangladesh"),
            array("val0"=>4,"val1"=>"BN","val2"=>"Brunei Darussalam"),
            array("val0"=>5,"val1"=>"KH","val2"=>"Cambodia"),
            array("val0"=>6,"val1"=>"CN","val2"=>"China"),
            array("val0"=>7,"val1"=>"HK","val2"=>"Hong Kong"),
            array("val0"=>8,"val1"=>"IN","val2"=>"India"),
            array("val0"=>9,"val1"=>"ID","val2"=>"Indonesia"),
            array("val0"=>10,"val1"=>"IR","val2"=>"Iran,Rebuplik Islam/ Iran,Islamic  Republic of "),
            array("val0"=>11,"val1"=>"IQ","val2"=>"Iraq"),
            array("val0"=>12,"val1"=>"JP","val2"=>"Jepun/ Japan"),
            array("val0"=>13,"val1"=>"JO","val2"=>"Jordan"),
            array("val0"=>14,"val1"=>"KP","val2"=>"Korea, Rebuplik Demokratik Rakyat/ Korea, Democratic People's Republic of"),
            array("val0"=>15,"val1"=>"KR","val2"=>"Republik Korea/ Korea, Republic of"),
            array("val0"=>16,"val1"=>"KW","val2"=>"Kuwait"),
            array("val0"=>17,"val1"=>"LA","val2"=>"Republik Demokratik Lao/ Lao, People's Democratic Republic"),
            array("val0"=>18,"val1"=>"LB","val2"=>"Lubnan/ Lebanon"),
            array("val0"=>19,"val1"=>"MV","val2"=>"Maldives"),
            array("val0"=>20,"val1"=>"MO","val2"=>"Macau"),
            array("val0"=>21,"val1"=>"MN","val2"=>"Mongolia"),
            array("val0"=>22,"val1"=>"MM","val2"=>"Myanmar"),
            array("val0"=>23,"val1"=>"NP","val2"=>"Nepal"),
            array("val0"=>24,"val1"=>"OM","val2"=>"Oman"),
            array("val0"=>25,"val1"=>"PK","val2"=>"Pakistan"),
            array("val0"=>26,"val1"=>"PH","val2"=>"Filipina/Philiphines"),
            array("val0"=>27,"val1"=>"QA","val2"=>"Qatar"),
            array("val0"=>28,"val1"=>"SA","val2"=>"Saudi Arabia"),
            array("val0"=>29,"val1"=>"SG","val2"=>"Singapura/Singapore"),
            array("val0"=>30,"val1"=>"LK","val2"=>"Sri Lanka"),
            array("val0"=>31,"val1"=>"SY","val2"=>"Republik Arab Syria / Syirian Arab Republic"),
            array("val0"=>32,"val1"=>"TH","val2"=>"Thailand"),
            array("val0"=>33,"val1"=>"TW","val2"=>"Taiwan"),
            array("val0"=>34,"val1"=>"TL","val2"=>"Timor-Laste"),
            array("val0"=>35,"val1"=>"TR","val2"=>"Turkey"),
            array("val0"=>36,"val1"=>"AE","val2"=>"United Arab Emirates"),
            array("val0"=>37,"val1"=>"VN","val2"=>"Viet Nam"),
            array("val0"=>38,"val1"=>"YE","val2"=>"Yemen"),
            array("val0"=>39,"val1"=>"BW","val2"=>"Botswana"),
            array("val0"=>40,"val1"=>"EG","val2"=>"Egypt"),
            array("val0"=>41,"val1"=>"KE","val2"=>"Kenya"),
            array("val0"=>42,"val1"=>"LY","val2"=>"Libyan Arab Jamahiriya"),
            array("val0"=>43,"val1"=>"NG","val2"=>"Nigeria"),
            array("val0"=>44,"val1"=>"SO","val2"=>"Somalia"),
            array("val0"=>45,"val1"=>"ZA","val2"=>"Afrika Selatan/ South Africa"),
            array("val0"=>46,"val1"=>"SD","val2"=>"Sudan"),
            array("val0"=>47,"val1"=>"TZ","val2"=>"Tanzania, Rebuplik Bersatu/ Tanzania, United Republic of"),
            array("val0"=>48,"val1"=>"DK","val2"=>"Denmark"),
            array("val0"=>49,"val1"=>"FI","val2"=>"Finland"),
            array("val0"=>50,"val1"=>"FR","val2"=>"Perancis/France"),
            array("val0"=>51,"val1"=>"DE","val2"=>"Jerman/Germany"),
            array("val0"=>52,"val1"=>"IT","val2"=>"Itali/Italy"),
            array("val0"=>53,"val1"=>"NL","val2"=>"Belanda/Netherlands"),
            array("val0"=>54,"val1"=>"NO","val2"=>"Norway"),
            array("val0"=>99,"val1"=>"MY","val2"=>"Malaysia"),
            array("val0"=>55,"val1"=>"RU","val2"=>"Rusia/Russian Federation"),
            array("val0"=>56,"val1"=>"ES","val2"=>"Sepanyol/Spain"),
            array("val0"=>57,"val1"=>"SE","val2"=>"Sweden"),
            array("val0"=>58,"val1"=>"CH","val2"=>"Switzerland"),
            array("val0"=>59,"val1"=>"GB","val2"=>"United Kingdom"),
            array("val0"=>60,"val1"=>"AU","val2"=>"Australia"),
            array("val0"=>61,"val1"=>"CA","val2"=>"Kanada/ Canada"),
            array("val0"=>62,"val1"=>"NZ","val2"=>"New Zealand"),
            array("val0"=>63,"val1"=>"PG","val2"=>"Papua New Guinea"),
            array("val0"=>64,"val1"=>"US","val2"=>"Amerika Syarikat/ United States")
        );
    }
}
