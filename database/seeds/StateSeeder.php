<?php

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items=$this->data();

        $countryMalaysia=\App\Country::where('code','malaysia')->first();

        foreach($items as $item){
            \App\State::updateOrCreate([
                'slug'=>str_slug($item['val2']),
            ],[
                'code'=>str_slug($item['val2'],'_'),
                'name'=>$item['val2'],
                'number_code'=>$item['val1'],
                'is_active'=>1,
                'country_id'=>$countryMalaysia->id,
            ]);
        }
    }

    protected function data(){
        return array(
            array("val0"=>1,"val1"=>'01',"val2"=>"Johor","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>2,"val1"=>'02',"val2"=>"Kedah","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>3,"val1"=>'03',"val2"=>"Kelantan","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>4,"val1"=>'04',"val2"=>"Melaka","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>5,"val1"=>'05',"val2"=>"Negeri Sembilan","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>6,"val1"=>'06',"val2"=>"Pahang","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>7,"val1"=>'07',"val2"=>"Pulau Pinang","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>8,"val1"=>'08',"val2"=>"Perak","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>9,"val1"=>'09',"val2"=>"Perlis","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>10,"val1"=>'10',"val2"=>"Selangor","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>11,"val1"=>'11',"val2"=>"Terengganu","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>12,"val1"=>'12',"val2"=>"Sabah","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>13,"val1"=>'13',"val2"=>"Sarawak","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>14,"val1"=>'14',"val2"=>"Wilayah Persekutuan Kuala Lumpur","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>15,"val1"=>'15',"val2"=>"Wilayah Persekutuan Labuan","val3"=>"NULL","val4"=>"NULL"),
            array("val0"=>16,"val1"=>'16',"val2"=>"Wilayah Persekutuan Putrajaya","val3"=>"NULL","val4"=>"NULL")
        );
    }
}

