<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=[
            'admin',
            'supervisor',
            'census enumerator',
        ];

        foreach($roles as $role){
            \App\Role::create(['guard_name' => 'web', 'name' => $role]);
        }
    }
}
