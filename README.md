# My Ipcs



###.env
APP_NAME=MyIpcs  
APP_ENV=  
APP_KEY=  
APP_DEBUG=true  
APP_URL=  
APP_WEB=true  
APP_HTTPS=false  
LOG_CHANNEL=daily  
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=  
DB_USERNAME=  
DB_PASSWORD=  
HASHID_SALT=D331C221C92E23663CE6AECF8AF8F  
HASHID_LENGTH=10  
APP_LOG=daily  
BROADCAST_DRIVER=log  
CACHE_DRIVER=file  
SESSION_DRIVER=file  
SESSION_LIFETIME=1200000  
QUEUE_DRIVER=sync  
REDIS_HOST=127.0.0.1  
REDIS_PASSWORD=null  
REDIS_PORT=6379  
MAIL_DRIVER=smtp  
MAIL_HOST=smtp.mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=  
MAIL_PASSWORD=  
MAIL_ENCRYPTION=null  
PUSHER_APP_ID=  
PUSHER_APP_KEY=  
PUSHER_APP_SECRET=  
PUSHER_APP_CLUSTER=mt1  
MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"  
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"  
ARCANEDEV_LOGVIEWER_MIDDLEWARE=web,auth  
SCOUT_QUEUE=  

####Database Guide

1. table name  
- should be plurals  
- should be in english so that its easier to make it plural  
- lower snake case
2. column name
- id column shall be named “id” autoincrement  
- should be singular most of the time  
- in english  
- as descriptive as possible, no short word,its ok if its long but not short  
- any foreign key column that reference ID should be appended with _id    
- lower snake case  
- date/datetime column preferable appended with _date _datetime  

####Laravel model/entity
- singular class/file name  
- camelcase class/file name  
- relationship  
- anything that relates to 1 record use singular name  
- anything that relates to many record(1 to many) use plurals  
- camelcase method name  
    