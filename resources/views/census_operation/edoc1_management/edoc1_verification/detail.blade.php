@extends('layouts.page_with_sidebar.main')
@section('content')
    <h1 class="page-header">Butiran Edoc 1  </h1>
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Butiran Tempat Kediaman
                    <div class="panel-heading-btn">
                        {{--@role('admin')--}}
                        {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                        {{--@endrole--}}

                    </div>
                </div>
                <div class="panel-body form-horizontal form-bordered">
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">No. UB</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                {!! $household->livingQuarter->living_quarter_information['ub_number']??'-' !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">No. TK/TKI/BTK</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                {!! $household->livingQuarter->living_quarter_information['tk_number']??'-' !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">ID Alamat</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Alamat</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Kod TK/TKI/BTK</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Jenis TK</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Butiran Isi Rumah
                    <div class="panel-heading-btn">
                        {{--@role('admin')--}}
                        {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                        {{--@endrole--}}

                    </div>
                </div>
                <div class="panel-body form-horizontal form-bordered">
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">No. IR</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
{{--                                {!! $household->householdHead->household_member_information['name']??'-' !!}--}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Nama ketua isi rumah</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Keturunan ketua isi rumah</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Bilangan penduduk pada hari banci</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Tarikh lawatan</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-15">
                        <label class="col-md-3 col-form-label text-right">Mod pembancian</label>
                        <div class="col-md-9">
                            <div class="form-control-plaintext">
                                -
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop
