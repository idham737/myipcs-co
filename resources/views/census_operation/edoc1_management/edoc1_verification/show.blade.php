@extends('layouts.page_with_sidebar.main')
@section('content')
    <h1 class="page-header">Edoc 1 <small>Tempat Kediaman</small>  </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-table"></i></div>
                <div class="stats-info">
                    <h4>Jumlah Tempat Kediaman</h4>
                    <p>{{$livingQuarters->count()}}</p>
                </div>
                <div class="stats-link">
                    {{--                    <a href="{{route($baseRoute.'.index',[])}}">Show all <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-table"></i></div>
                <div class="stats-info">
                    <h4>Jumlah Isi Rumah</h4>
                    <p>{{$households->count()}}</p>
                </div>
                <div class="stats-link">
                    {{--                    <a href="{{route($baseRoute.'.index',[])}}">Show all <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-table"></i></div>
                <div class="stats-info">
                    <h4>Jumlah Ahli Isi Rumah</h4>
                    <p>{{$householdMembers->count()}}</p>
                </div>
                <div class="stats-link">
                    {{--                    <a href="{{route($baseRoute.'.index',[])}}">Show all <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-user"></i>
                    Maklumat Pembanci
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">
                    <dl>
                        <dt class="text-inverse">Nama</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">No Telefon</dt>
                        <dd>-</dd>
                    </dl>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-building"></i>
                    Butir Butir Pengenalan Blok Penghitungan
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">
                    <dl>
                        <dt class="text-inverse">Negeri</dt>
                        <dd>{!! $enumerationBlock->censusCircle->censusDistrict->administrativeDistrict->state->name??'-' !!}</dd>
                        <dt class="text-inverse">Daerah Pentadbiran</dt>
                        <dd>{!! $enumerationBlock->censusCircle->censusDistrict->administrativeDistrict->name??'-' !!}</dd>
                        <dt class="text-inverse">Mukim</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Strata</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Daerah Banci</dt>
                        <dd>{!! $enumerationBlock->censusCircle->censusDistrict->number_code??'-' !!}</dd>
                        <dt class="text-inverse">Lingkungan Banci</dt>
                        <dd>{!! $enumerationBlock->censusCircle->number_code??'-' !!}</dd>
                        <dt class="text-inverse">Blok Penghitungan</dt>
                        <dd>{!! $enumerationBlock->number_code??'-' !!}</dd>



                    </dl>
                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-chart-line"></i>
                    Ringkasan Blok Penghitungan
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">
                    <dl>

                        <dt class="text-inverse">Tempat Kediaman</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Didiami</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Kosong</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Isi Rumah</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Melayu</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Bumiputera lain:-</dt>
                        <dt class="text-inverse">Cina</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">India</dt>
                        <dd>-</dd>
                        <dt class="text-inverse">Bukan bumiputera:-</dt>
                        <dt class="text-inverse"></dt>
                        <dd>-</dd>

                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Senarai tempat kediaman mengikut kumpulan isi rumah
                    <div class="panel-heading-btn">
                        {{--@role('admin')--}}
                        {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                        {{--@endrole--}}

                    </div>
                </div>
                <div class="panel-body">
                    <table width="100%" class="datatable table table-bordered table-condensed table-striped">
                        <thead>
                        <tr>
                            <td width="1%">ID</td>
                            <td>No. UB</td>
                            <td>No. TK/TKI/BTK</td>
                            <td>KIR</td>
                            <td>Keturunan KIR</td>
                            <td>Bil. Penduduk</td>
                            <td>Status</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($households as $index=>$item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->livingQuarter->living_quarter_information['ub_number']??'-'}}</td>
                                <td>{{$item->livingQuarter->living_quarter_information['living_quarter_number']??'-'}}</td>
                                <td>{{$item->householdHead->household_member_information['name']??'-'}}</td>
                                <td>{{$item->householdHead->household_member_information['race']??'-'}}</td>
                                <td>{{$item->householdHead->household_member_information['bil']??'-'}}</td>
                                <td>
                                    <span class="label label-{{$item->status->class??''}}">{{$item->status->name2??''}}</span>
                                </td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="{{route('census-operation.edoc1-management.edoc1-verifications.detail',[$item->id])}}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
