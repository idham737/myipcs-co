@extends('layouts.page_with_sidebar.main')
@section('content')
    <h1 class="page-header">Edoc 1 <small>Blok Penghitungan</small>  </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-table"></i></div>
                <div class="stats-info">
                    <h4>Blok Penghitungan</h4>
                    <p>{{$models->count()}}</p>
                </div>
                <div class="stats-link">
{{--                    <a href="{{route($baseRoute.'.index',[])}}">Show all <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Senarai Blok Penghitungan
                    <div class="panel-heading-btn">
                        {{--@role('admin')--}}
                        {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                        {{--@endrole--}}

                    </div>
                </div>
                <div class="panel-body">
                    <table width="100%" class="datatable table table-striped">
                        <thead>
                        <tr>
                            <td width="1%">ID</td>
                            <td>Blok Penghitungan</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($models as $index=>$item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->number_code}}</td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="{{route($baseRoute.'.show',[$item->id])}}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
