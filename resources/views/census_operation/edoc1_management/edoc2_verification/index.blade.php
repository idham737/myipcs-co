@extends('layouts.page_with_sidebar.main')
@section('content')
    <h1 class="page-header">Edoc 2  </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon"><i class="fa fa-table"></i></div>
                <div class="stats-info">
                    <h4>Jumlah Isi Rumah</h4>
                    <p>{{$household->householdMembers()->count()}}</p>
                </div>
                <div class="stats-link">
{{--                    <a href="{{route($baseRoute.'.index',[])}}">Show all <i class="fa fa-arrow-alt-circle-right"></i></a>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-user"></i>
                    Maklumat Tempat Kediaman
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">

                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-user"></i>
                    Maklumat Ketua Isi Rumah
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">

                </div>
            </div>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-user"></i>
                    Ringkasan Blok Penghitungan
                    <div class="panel-heading-btn">

                    </div>
                </div>
                <div class="panel-body">

                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <i class="fa fa-list"></i>
                    Senarai tempat kediaman mengikut kumpulan isi rumah
                    <div class="panel-heading-btn">
                        {{--@role('admin')--}}
                        {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                        {{--@endrole--}}

                    </div>
                </div>
                <div class="panel-body">
                    <table width="100%" class="datatable table table-striped">
                        <thead>
                        <tr>
                            <td width="1%">ID</td>
                            <td>No. UB</td>
                            <td>No. TK/TKI/BTK</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($households as $index=>$item)
                            <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->livingQuarter->living_quarter_information['ub_number']??'-'}}</td>
                                <td>{{$item->livingQuarter->living_quarter_information['living_quarter_number']??'-'}}</td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="{{route('census-operation.edoc1-management.edoc2-verifications.index',[$item->id])}}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
