@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body" style="background-color:#F5F5F5"><br>
                  <center><h2 style="color:#575757">RINGKASAN PENDUDUK MENGIKUT JANTINA</h2></center>
                  <center><h2 style="color:#298A00">NEGERI</h2></center><br>
                  <div class="container">
                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">PULAU PINANG</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[0]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[0]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">KEDAH</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[1]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[1]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">PERLIS</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[2]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[2]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">PERAK</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[3]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[3]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br> 
                    
                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">SELANGOR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[4]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[4]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">W.P. KUALA LUMPUR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[5]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[5]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">MELAKA</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[6]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[6]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">N. SEMBILAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[7]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[7]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br>

                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">JOHOR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[8]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[8]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">TERENGGANU</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[9]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[9]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">PAHANG</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[10]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[10]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">KELANTAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[11]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[11]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br>

                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">SABAH</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[12]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[12]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">SARAWAK</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[13]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[13]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">W.P. PUTRAJAYA</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[14]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[14]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="{{ route('genderAD') }}"><h4 style="color:#666666">W.P. LABUAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><h2 style="color:#6DBBFF">L</h2></td>
                              <td><h2 style="color:#FFB2B2">P</h2></td>
                            </tr>
                            <tr>
                              <td><h2 style="color:#6DBBFF">{{ $gender_st[15]->total_male }}</h1></td>
                              <td><h2 style="color:#FFB2B2">{{ $gender_st[15]->total_female }}</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection