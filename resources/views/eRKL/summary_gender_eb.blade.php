@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body"><br>
                  <center><h2 style="color:#575757">RINGKASAN PENDUDUK MENGIKUT JANTINA</h2></center>
                  <center><h3 style="color:#298A00">BLOK PENGHITUNGAN</h3></center><br>
                  <div class="container">
                      <a style="color: black">
                        <table width="100%" class="datatable table table-striped">
                            <thead>
                            <tr>
                                <td width="1%">No.</td>
                                <td>Blok Penghitungan</td>
                                <td>Jumlah Lelaki</td>
                                <td>Jumlah Perempuan</td>
                            </tr>
                            </thead>
                            <tbody>
                              @foreach($gender_eb as $key=>$g)
                                <tr>
                                    <td>{{ ++$key }}</td>
                                    <td>{{ $g->eb_id }}</td>
                                    <td>{{ $g->total_male }}</td>
                                    <td>{{ $g->total_female }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table><br>
                      </a>         
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection