<style>.custom {
    width: 85px !important;
  }
  </style>
@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="card-body" style="background-color:white"><br>
    <form method="POST" action="{{ route('worklist_update') }}">
            {{ csrf_field() }}
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <i class="fa fa-list"></i>
            Senarai Tugas
            <div class="panel-heading-btn">
                <button type="submit" class="btn btn-default btn-xs custom">Sah</button>
                <a href="" class="btn btn-default btn-xs custom">Jana eDoc 24</a>
             

            </div>
        </div>
    </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-4">
                                 
                                </div>
                               
                            </div><br>
                            
                            <table width="100%" class="datatable table table-striped">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th style="width:400px">Nama Dokumen</th>
                                        <th style="width:300px">Nama Petugas</th>
                                        <th style="width:100px">Status</th>
                                        <th style="text-align:center" width="200px">Tindakan&nbsp;<input class="form-check-input" type="checkbox" id="checkAll"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($worklist_edoc24 as $key=>$w)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td><a  style="color:black" href="{{ route('edoc24',$w->id) }}">{{ $w ->document_name }}</a></td>
                                            <td>{{ $w->person_in_charge }}</td>
                                            <td>{{ $w->status }}</td>
                                            <td style="text-align: center"><input class="form-check-input" type="checkbox" value="{{ $w->id }}" name="update[]"></td>
                                            

                                           
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table><br>                        
                        </div>
    </form>
                  </div>

                  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
                  <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js'></script>
                  <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/123941/rwd-table-patterns.js'></script> 

                    <script type="text/javascript">
                    $("#checkAll").click(function () {
                    $('input:checkbox').not(this).prop('checked', this.checked);
                    });
                </script> 
@endsection