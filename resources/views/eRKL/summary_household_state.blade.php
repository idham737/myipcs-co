
@extends('layouts.page_with_sidebar.main')
@section('content')
<div class="container-fluid">
        <div class="animated fadeIn">
          <div class="row">
            <div class="col-lg-12">
              <div class="card" style="background-color:#F5F5F5">
                <!-- <div class="card-header">
                <h2>Tetapan Temu Janji</h2> -->
                  <!-- <div class="card-header-actions">
                    <a class="card-header-action" href="http://coreui.io/docs/components/bootstrap-breadcrumb/" target="_blank">
                      <small class="text-muted">docs</small>
                    </a>
                  </div> -->
                <!-- </div> -->
                <div class="card-body"><br>
                      <center><h1 style="color:#575757">JUMLAH ISI RUMAH</h1></center>
                      <center><h2 style="color:#298A00">NEGERI</h2></center><br>
                      <div class="container">
                        <div class="card-deck">
                          <div class="card">
                            <a href="{{ route('householdAD') }}">
                              <table>
                                <thead>
                                  <tr><br>
                                    <h3 class="text-center" style="color:#298A00">{{ $household_st[0]->total_a }}juta</h3>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <h5 class="text-primary text-center">PERLIS</h5>
                                  </tr>
                                  <tr>
                                    <h5 class="text-danger text-center">{{ $household_st[0]->total_b }}juta</h5>
                                  </tr>
                                </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[1]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">KEDAH</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[1]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[2]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">P.PINANG</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[2]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[3]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">MELAKA</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[3]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>
                        </div><br> 
                        
                        <div class="card-deck">
                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[4]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">JOHOR</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[4]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[5]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">TERENGGANU</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[5]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[6]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">PERAK</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[6]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[7]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">SELANGOR</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[7]->total_a }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>
                        </div><br>

                        <div class="card-deck">
                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[8]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">W.P. KUALA LUMPUR</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[8]->total_a }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[9]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">N. SEMBILAN</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[9]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[10]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">PAHANG</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[10]->total_b }}juta</h5>
                                </tr>
                              </tbody>
                            </table>
                            </a>
                          </div>

                          <div class="card">
                            <a href="co\erkl_penduduk_dp.html">
                              <table>
                              <thead>
                                <tr><br>
                                  <h3 class="text-center" style="color:#298A00">{{ $household_st[11]->total_a }}juta</h3>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <h5 class="text-primary text-center">KELANTAN</h5>
                                </tr>
                                <tr>
                                  <h5 class="text-danger text-center">{{ $household_st[11]->total_a }}juta</h5>
                                </tr>
                              </tbody>
                              </table>
                            </a>
                          </div>  
                        </div><br>

                        <div class="card-deck">

                            <div class="card">
                              <a href="co\erkl_penduduk_dp.html">
                                <table>
                                  <thead>
                                    <tr><br>
                                      <h3 class="text-center" style="color:#298A00">{{ $household_st[12]->total_a }}juta</h3>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <h5 class="text-primary text-center">W.P. LABUAN</h5>
                                    </tr>
                                    <tr>
                                      <h5 class="text-danger text-center">{{ $household_st[12]->total_b }}juta</h5>
                                    </tr>
                                  </tbody>
                                </table>
                              </a>
                            </div> 

                            <div class="card">
                              <a href="co\erkl_penduduk_dp.html">
                                <table>
                                <thead>
                                  <tr><br>
                                    <h3 class="text-center" style="color:#298A00">{{ $household_st[13]->total_a }}juta</h3>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <h5 class="text-primary text-center">W.P. PUTRAJAYA</h5>
                                  </tr>
                                  <tr>
                                    <h5 class="text-danger text-center">{{ $household_st[13]->total_b }}juta</h5>
                                  </tr>
                                </tbody>
                                </table>
                              </a>
                            </div>

                            <div class="card">
                              <a href="co\erkl_penduduk_dp.html">
                                <table>
                                <thead>
                                  <tr><br>
                                    <h3 class="text-center" style="color:#298A00">{{ $household_st[14]->total_a }}juta</h3>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <h5 class="text-primary text-center">SARAWAK</h5>
                                  </tr>
                                  <tr>
                                    <h5 class="text-danger text-center">{{ $household_st[14]->total_a }}juta</h5>
                                  </tr>
                                </tbody>
                                </table>
                              </a>
                            </div>

                            <div class="card">
                              <a href="co\erkl_penduduk_dp.html">
                                <table>
                                <thead>
                                  <tr><br>
                                    <h3 class="text-center" style="color:#298A00">{{ $household_st[15]->total_a }}juta</h3>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <h5 class="text-primary text-center">SABAH</h5>
                                  </tr>
                                  <tr>
                                    <h5 class="text-danger text-center">{{ $household_st[15]->total_a }}juta</h5>
                                  </tr>
                                </tbody>
                                </table>
                              </a>
                            </div> 
                          </div>
                      </div>
                </div>
              </div>
              
            </div>
          </div>
          <!-- /.row-->
        </div>
      </div>
      @endsection