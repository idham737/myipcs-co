@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body" style="background-color:#F5F5F5"><br>
                  <center><h2 style="color:#575757">RINGKASAN PENDUDUK MENGIKUT KEWARGANEGARAAN</h2></center>
                  <center><h2 style="color:#298A00">NEGERI</h2></center><br>
                  <div class="container">
                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">PULAU PINANG</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">KEDAH</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href="l"><h4 style="color:#666666">PERLIS</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">PERAK</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br> 
                    
                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">SELANGOR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">W.P. KUALA LUMPUR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">MELAKA</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">N. SEMBILAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br>

                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">JOHOR</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">TERENGGANU</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">PAHANG</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">KELANTAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div><br>

                    <div class="card-deck">
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">SABAH</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">SARAWAK</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">W.P. PUTRAJAYA</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="card">
                        <table class="text-center">
                          <thead>
                            <tr>
                              <td colspan="2" style="background-color:#E2FFC8"><a style="color:white" href=""><h4 style="color:#666666">W.P. LABUAN</h4></a></td>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td style="font-size:10px;color:#6DBBFF">WARGANEGARA</td>
                              <td class="text-danger" style="font-size:10px;color:#FFB2B2">BUKAN<br> WARGANEGARA</td>
                            </tr>
                            <tr>
                              <td><h1 style="color:#6DBBFF">00</h1></td>
                              <td><h1 style="color:#FFB2B2">00</h1></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> 
                    </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection