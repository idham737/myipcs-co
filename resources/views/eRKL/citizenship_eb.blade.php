@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body"><br>
                  <center><h2 style="color:#575757">RINGKASAN PENDUDUK MENGIKUT KEWARGANEGARAAN</h2></center>
                  <center><h3 style="color:#298A00">BLOK PENGHITUNGAN</h3></center><br>
                  <div class="container">
                      <a style="color: black" href="">
                        <table width="100%" class="datatable table table-striped">
                            <thead>
                            <tr>
                                <td width="1%">No.</td>
                                <td>Blok Penghitungan</td>
                                <td>Jumlah Warganegara</td>
                                <td>Jumlah Bukan Warganegara</td>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>XX</td>
                                    <td>XX</td>
                                    <td>XX</td>
                                </tr>
                                <tr>
                                  <td>2</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                                <tr>
                                  <td>3</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                                <tr>
                                  <td>4</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                                <tr>
                                  <td>5</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                                <tr>
                                  <td>6</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                                <tr>
                                  <td>7</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                  <td>XX</td>
                                </tr>
                            </tbody>
                        </table><br>
                      </a>         
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection