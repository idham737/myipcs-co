<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
 



</style>
</head>
<body>
    <button onclick="myFunction()">Print this page</button>
    <div class="container-fluid">
 <br><br>  

<h3><center><b>BANCI PENDUDUK DAN PERUMAHAN MALAYSIA 2020</b></center></h3>
<br>
<div class="col-md-6">
    <p><b>NEGERI (Nama dan Kod) :</b></p>
    
    <p><b>DAERAH PENTADBIRAN JAJAHAN (Nama dan Kod) :</b></p>
    
    <p><b>NOMBOR DAERAH BANCI (DB) :</b></p>

    <p><b>NOMBOR LINGKUNGAN BANCI (LB) :</b></p>
    </div>
    <table>
      <tr>
       <th rowspan="7">Nombor BP</th>
       <th colspan="3"><center>Tempat Kediaman</center></th>
       <th><center>Isi Rumah</center></th>
       <th colspan="21" ><center>Penduduk (Hari Banci)</center></th>
      </tr>
    
      <tr>
        <th rowspan="6">Didiami</th>
        <th rowspan="6">Kosong</th>
        <th rowspan="6">Jumlah</th>
        <th rowspan="6">Jumlah</th>
      </tr>
      <tr>
          <th rowspan="4">Jumlah</th>
          <th colspan="17"><center>Warganegara Malaysia</center></th>
          <th rowspan="3"colspan="3"><center>Bukan warganegara Malaysia</center></th>
         
         
        </tr>
      <tr>
        <th rowspan="3">Jumlah</th>
        <th colspan="7"><center>Bumiputera</center></th>
        <th rowspan="2" colspan="3"><center>Cina</center></th>
        <th rowspan="2"colspan="3"><center>India</center></th>
        <th rowspan="2"colspan="3"><center>Lain-lain</center></th>
        
       
      </tr>
      <tr>
        <th rowspan="2">Jumlah</th>
        <th colspan="3"><center>Melayu</center></th>
        <th colspan="3">Bumiputera Lain</th>
      </tr>
  
     <tr>
       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>

       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>

       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>

       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>
       
       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>

       <th width="4%">L</th>
       <th width="4%">P</th>
       <th>Jumlah</th>
     </tr>
   
    <tr>
      <td>(a)=(b)+(u)</td>
      <td>(b)=(c)+(l)+(o)+(r)</td>
      <td>(c)=(f)+(i)</td>
      <td>(d)</td>
      <td>(e)</td>
      <td>(f)=(d)+(e)</td>
      <td>(g)</td>
      <td>(h)</td>
      <td>(i)=(g)+(h)</td>
      <td>(j)</td>
      <td>(k)</td>
      <td>(l)=(j)+(k)</td>
      <td>(m)</td>
      <td>(n)</td>
      <td>(o)=(m)+(n)</td>
      <td>(p)</td>
      <td>(q)</td>
      <td>(r)=(p)+(q)</td>
      <td>(s)</td>
      <td>(t)</td>
      <td>(u)=(s)+(t)</td>
    </tr>

    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
          <td><b>JUMLAH</b></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>

    </table>
<br>
    <div class="row">
        <div class="col-sm-8">
            <h5>NOTA :</h5>
            <p>(1) Borang ini hendaklah diisi  oleh Penyelia berasakan kepada muka surat R1/R2,Dokumen 1.</p>
            <p>(2) Sila lengkapkan borang ini dalam dua salinan dan serahkannya kepada Penguasa Daerah.</p>
        </div>
        <div class="col-sm-4">
            
            <p>Nama Penyelia :</p>
            <p>Tarikh :</p>
        </div>
       

    </div>
</body>
</html>


<script>
function myFunction() {
  window.print();
}
</script>
