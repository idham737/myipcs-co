<div id="top-menu" class="top-menu">
    <!-- begin top-menu nav -->
    <ul class="nav">
        <li class="has-sub {{request()->segment(2)=='dashboard'?'active':''}}"><a href="{{route('control-panel.dashboard.index')}}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <li class="has-sub {{request()->segment(2)=='products'?'active':''}}"><a href="{{route('control-panel.products.index')}}"><i class="fa fa-list"></i> <span>Product</span></a></li>
        <li class="has-sub {{request()->segment(2)=='inquiries'?'active':''}}"><a href="{{route('control-panel.inquiries.index')}}"><i class="fa fa-list"></i> <span>Inquiry</span></a></li>
        @if(auth()->user()->hasRole('admin'))
        <li class="has-sub {{request()->segment(2)=='users'?'active':''}}"><a href="{{route('control-panel.users.index')}}"><i class="fa fa-users"></i> <span>User</span></a></li>
        <li class="has-sub {{request()->segment(2)=='brands'?'active':''}}"><a href="{{route('control-panel.brands.index')}}"><i class="fa fa-cogs"></i> <span>Brand</span></a></li>
        <li class="has-sub {{request()->segment(2)=='merchants'?'active':''}}"><a href="{{route('control-panel.merchants.index')}}"><i class="fa fa-users"></i> <span>Merchant</span></a></li>
        <li class="has-sub {{request()->segment(2)=='product-categories'?'active':''}}"><a href="{{route('control-panel.product-categories.index')}}"><i class="fa fa-cogs"></i> <span>Product Category</span></a></li>
        <li class="has-sub {{request()->segment(2)=='product-subscriptions'?'active':''}}"><a href="{{route('control-panel.product-subscriptions.index')}}"><i class="fa fa-cogs"></i> <span>Product Subscription</span></a></li>
        @endif

        <li class="has-sub"><a href="{{route('home.index')}}" target="_blank"><i class="fa fa-globe"></i> <span>Go To Website</span></a></li>

    </ul>
    <!-- end top-menu nav -->
</div>
