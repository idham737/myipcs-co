@extends('layouts.core')

@push('_styles')
    @stack('styles')
@endpush

@push('_scripts')
    @stack('scripts')
@endpush

@section('main_content')
    <div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed page-with-top-menu">
    <!-- begin #header -->
        @include('layouts._header')
        @include('layouts._top_menu')
        @include('layouts._sidebar')
        @hasSection('content')
            <div id="content" class="content">
                @yield('content')
            </div>
    @endif
    <!-- end #content -->

        <!-- begin theme-panel -->

        <!-- end theme-panel -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>


@endsection
