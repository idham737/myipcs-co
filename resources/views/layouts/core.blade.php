<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    @if (isset($pageTitle))
        <title>{{ $pageTitle }}</title>
    @else
        <title>{{ env('APP_NAME') }}</title>
    @endif

<!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="{{config('core.https')?'https':'http'}}://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{asset('assets/plugins/jquery-ui/jquery-ui.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
    {{--<link href="{{asset('assets/plugins/font-awesome/5.0/css/fontawesome.css',config('core.https'))}}" rel="stylesheet" />--}}
    <link href="{{asset('assets/plugins/font-awesome/5.0/css/fontawesome-all.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/animate/animate.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/css/default/style.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/css/default/style-responsive.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/css/default/theme/blue.css',config('core.https'))}}" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/gritter/css/jquery.gritter.css',config('core.https'))}}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('assets/plugins/pace/pace.min.js',config('core.https'))}}"></script>
    <!-- ================== END BASE JS ================== -->


    <link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/ionRangeSlider/css/ion.rangeSlider.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/password-indicator/css/password-indicator.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/jquery-tag-it/css/jquery.tagit.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/select2/dist/css/select2.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css',config('core.https'))}}" rel="stylesheet" />

    <link href="{{asset('plugins/fullcalendar-3.9.0/fullcalendar.css',config('core.https'))}}" rel="stylesheet" />
    <link href="{{asset('plugins/Highcharts-6.1.1/code/css/highcharts.css',config('core.https'))}}" rel="stylesheet" />

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    {{--<link href="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.css',config('core.https'))}}" rel="stylesheet" />--}}
<!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <!-- ================== END BASE JS ================== -->

    <style>
        div.required .control-label:after,label.required:after {
            content:"*";
            color:red;
        }
        .reset-a, .reset-a:hover, .reset-a:visited, .reset-a:focus, .reset-a:active  {
            text-decoration: none;
            color: inherit;
            outline: 0;
            cursor: auto;
        }

        .bigger-form-font{
            font-size: 14px !important;
        }

        @media (max-width: 767px) {
            .text-right { text-align:left !important; }
        }
        .form-control-plaintext {
            display: block;
            width: 100%;
            height: auto;
            margin-bottom: 0;
            line-height: 1.42857143;
            padding: 6px 0;
            background-color: transparent;
            border: solid transparent;
            border-width: 1px 0;
        }
    </style>

    @stack('_styles')
</head>
<body >
<div id="page-loader" class="fade show"><span class="spinner"></span></div>

@yield('main_content')

@include('_blocks.plugins.jsvalidation')
@include('_blocks.generic_script')
@include('_blocks.plugins.select2')
@include('_blocks.plugins.chosen')

@stack('modals')
<script src="{{asset('assets/plugins/jquery/jquery-3.2.1.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js',config('core.https'))}}"></script>

<!-- ================== BEGIN BASE JS ================== -->
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js',config('core.https'))}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js',config('core.https'))}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/js-cookie/js.cookie.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/theme/default.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/apps.js',config('core.https'))}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/plugins/gritter/js/jquery.gritter.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.resize.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.pie.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/demo/dashboard.min.js',config('core.https'))}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',config('core.https'))}}"></script>


<!-- ================== END PAGE LEVEL JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/masked-input/masked-input.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/password-indicator/js/password-indicator.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-tag-it/js/tag-it.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/moment.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/select2/dist/js/select2.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-show-password/bootstrap-show-password.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/clipboard/clipboard.min.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/fullcalendar-3.9.0/fullcalendar.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/Highcharts-6.1.1/code/js/highcharts.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/Highcharts-6.1.1/code/js/highcharts-3d.js',config('core.https'))}}"></script>
{{--<script src="{{asset('assets/js/form-plugins.demo.min.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.esm.js',config('core.https'))}}"></script>--}}
{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.common.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/vuejs-datepicker/dist/vuejs-datepicker.common.js',config('core.https'))}}"></script>--}}

<script src="{{asset('plugins/vuejs-datepicker/dist/vuejs-datepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('js/moment.js',config('core.https'))}}"></script>
<script src="{{asset('js/vue.js',config('core.https'))}}"></script>


<!-- ================== END PAGE LEVEL JS ================== -->
<!-- Data tables -->
<script>
    $.fn.datepicker.defaults.autoclose = true;
    $.extend( true, $.fn.dataTable.defaults, {
        "language": {
            "decimal":        "",
            "emptyTable":     "Tiada rekod dijumpai",
            "info":           "Menunjukkan _START_ hingga _END_ dari _TOTAL_ rekod",
            "infoEmpty":      "Menunjukkan 0 to 0 of 0 rekod",
            "infoFiltered":   "(ditapis dari _MAX_ jumlah rekod)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Tunjuk _MENU_ rekod",
            "loadingRecords": "Loading...",
            "processing":     "Memproses...",
            "search":         "Carian:",
            "zeroRecords":    "Tiada rekod dijumpai",
            "paginate": {
                "first":      "Pertama",
                "last":       "Terakhir",
                "next":       "Seterusnya",
                "previous":   "Sebelum"
            },
            "aria": {
                "sortAscending":  ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        },

    } );

</script>
<script>


    $(document).ready(function() {
        App.init();
    });
</script>

{{--@if(session()->has('snackbar'))--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function ()--}}
        {{--{--}}
            {{--new PNotify({--}}
                {{--text    : '{{session('snackbar')}}',--}}
                {{--confirm : {--}}
                    {{--confirm: true,--}}
                    {{--buttons: [--}}
                        {{--{--}}
                            {{--text    : 'Dismiss',--}}
                            {{--addClass: 'btn btn-link',--}}
                            {{--click   : function (notice)--}}
                            {{--{--}}
                                {{--notice.remove();--}}
                            {{--}--}}
                        {{--},--}}
                        {{--null--}}
                    {{--]--}}
                {{--},--}}
                {{--buttons : {--}}
                    {{--closer : false,--}}
                    {{--sticker: false--}}
                {{--},--}}
                {{--animate : {--}}
                    {{--animate  : true,--}}
                    {{--in_class : 'slideInDown',--}}
                    {{--out_class: 'slideOutUp'--}}
                {{--},--}}
                {{--addclass: 'md'--}}
            {{--});--}}

        {{--});--}}

    {{--</script>--}}
{{--@endif--}}

<script>
    (function ()
    {
        $(document).ready(function ()
        {
            $('.datatable-layout-2').DataTable(
                {
                    dom         : 'Brtip',
                    initComplete: function ()
                    {
                        var api = this.api(),
                            searchBox = $('#datatable-layout-2-search-input');

                        // Bind an external input as a table wide search box
                        if ( searchBox.length > 0 )
                        {
                            searchBox.on('keyup', function (event)
                            {
                                api.search(event.target.value).draw();
                            });
                        }
                    },
                    lengthMenu  : [10, 20, 30, 50, 100],
                    pageLength  : 10,
                    scrollY     : 'auto',
                    scrollX     : false,
                    responsive  : true,
                    autoWidth   : false,
                    buttons: [
                        'copy', 'excel', 'pdf'
                    ],
                    "order": []
                }
            );



            $('.datatable-showall').DataTable({
                "aLengthMenu": [[-1], ["All"]],
                "bFilter": true,
                responsive: true,
                "order": [],
            });

            $('.datatable').DataTable({
                responsive: true,
                columnDefs: [
                    { responsivePriority: 1, targets: -1 },
                ],
                "order": [],
            });

            $('.datatable-exportable').DataTable({
                // "bFilter": true,
                "dom": '<B>T<"clear">lfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel', 'pdf'
                ],
                "order": []
            });


        });
    })();
</script>
@if(session()->has('modal'))
    <script type="text/javascript">
        $(document).ready(function ()
        {
            $('#{{session('modal')}}').modal('show');

        });

    </script>
@endif

@if(session()->has('openTab'))
    <script type="text/javascript">
        $(document).ready(function ()
        {
            {{--$('{{session('openTab')}}').tab('show');--}}
            console.log('opentab','{{session('openTab')}}');
            $('a[href="#{{session('openTab')}}"]').tab('show')
        });

    </script>
@endif

<script>
    $('#flash-overlay-modal').modal();
</script>


<script>
    $(document).ready(function(){
        $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY hh:mm A'
        });

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });



        $('.datetimepickerstart').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY hh:mm A'
        });
        $('.datetimepickerend').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY hh:mm A'
        });
        $(".datetimepickerstart").on("dp.change", function (e) {
            $('.datetimepickerend').data("DateTimePicker").minDate(e.date);
        });
    })

    $(window).scroll(function() {
        sessionStorage.scrollTop = $(this).scrollTop();
    });

    $(document).ready(function() {
        if (sessionStorage.scrollTop != "undefined") {
            $(window).scrollTop(sessionStorage.scrollTop);
        }
    });
</script>


{{--<script type="text/javascript">--}}
    {{--$(document).ready(function ()--}}
    {{--{--}}


        {{--$('.invalid-feedback').each(function(){--}}
            {{--var dataInputName=$(this).data('input-name');--}}
            {{--var dataMessageBagName=$(this).data('message-bag-name');--}}

            {{--var selectorString='';--}}
            {{--if(dataInputName){--}}
                {{--selectorString+='[name="'+dataInputName+'"]';--}}
            {{--}--}}

            {{--if(dataMessageBagName!==''){--}}
                {{--selectorString+='[data-message-bag-name="'+dataMessageBagName+'"]';--}}
            {{--}--}}

            {{--$(selectorString).addClass('is-invalid');--}}

        {{--})--}}

    {{--});--}}

{{--</script>--}}


{{--<script src="{!! asset('plugins/jquery.growl/javascripts/jquery.growl.js') !!}" type="text/javascript"></script>--}}
{{--<link href="{!! asset('plugins/jquery.growl/stylesheets/jquery.growl.css') !!}" rel="stylesheet" type="text/css" />--}}

{{--@if(session()->has('growl'))--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function ()--}}
        {{--{--}}
            {{--$.growl({message: '{{session('growl')}}' });--}}
        {{--});--}}

    {{--</script>--}}
{{--@endif--}}
{{--@if(session()->has('growlError'))--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function ()--}}
        {{--{--}}
            {{--$.growl.error({message: '{{session('growlError')}}' });--}}
        {{--});--}}

    {{--</script>--}}
{{--@endif--}}
{{--@if(session()->has('growlInfo'))--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function ()--}}
        {{--{--}}
            {{--$.growl.notice({message: '{{session('growlInfo')}}' });--}}
        {{--});--}}

    {{--</script>--}}
{{--@endif--}}

{{--<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>--}}
{{--{!! Toastr::message() !!}--}}

<script src="http://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
@include('sweetalert::alert')

@if(isset($jsValidators))
    @foreach($jsValidators as $jsValidatorItem)
        {!! $jsValidatorItem['validator']->selector($jsValidatorItem['selector']) !!}
    @endforeach
@endif
@if(isset($jsValidator))
    {!! $jsValidator !!}
@endif

@stack('_scripts')
</body>
</html>

