<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        {{--<ul class="nav">--}}
            {{--<li class="nav-profile">--}}
                {{--<a href="javascript:;" data-toggle="nav-profile">--}}
                    {{--<div class="cover with-shadow"></div>--}}
                    {{--<div class="image">--}}
                        {{--<img src="../assets/img/user/user-13.jpg" alt="" />--}}
                    {{--</div>--}}
                    {{--<div class="info">--}}
                        {{--<b class="caret pull-right"></b>--}}
                        {{--Sean Ngu--}}
                        {{--<small>Front end developer</small>--}}
                    {{--</div>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li>--}}
                {{--<ul class="nav nav-profile">--}}
                    {{--<li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>--}}
                    {{--<li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>--}}
                    {{--<li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        {{--</ul>--}}
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigasi</li>
            <li class="has-sub {{request()->segment(2)=='dashboard'?'active':''}}"><a href="{{route('dashboard.index')}}"><i class="fa fa-home"></i> <span>Papan pemuka</span></a></li>
            <li class="has-sub {{request()->segment(2)=='edoc1-management'?'active':''}}"><a href="{{route('census-operation.edoc1-management.edoc1-verifications.index')}}"><i class="fa fa-file-pdf"></i> <span>Pengurusan eDoc1</span></a></li>
            {{--<li class="has-sub {{request()->segment(2)=='products'?'active':''}}"><a href="{{route('control-panel.products.index')}}"><i class="fa fa-list"></i> <span>Product</span></a></li>--}}
            {{--<li class="has-sub {{request()->segment(2)=='inquiries'?'active':''}}"><a href="{{route('control-panel.inquiries.index')}}"><i class="fa fa-list"></i> <span>Inquiry</span></a></li>--}}
            {{--<li class="has-sub {{request()->segment(2)=='credit-packages'?'active':''}}"><a href="{{route('control-panel.credit-packages.index')}}"><i class="fa fa-list"></i> <span>Credit Package</span></a></li>--}}
            {{--<li class="has-sub {{request()->segment(2)=='order-histories'?'active':''}}"><a href="{{route('control-panel.order-histories.index')}}"><i class="fa fa-list"></i> <span>Order History</span></a></li>--}}
            {{--@if(auth()->user()->hasRole('admin'))--}}
                {{--<li class="has-sub {{request()->segment(2)=='users'?'active':''}}"><a href="{{route('control-panel.users.index')}}"><i class="fa fa-users"></i> <span>User</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(2)=='brands'?'active':''}}"><a href="{{route('control-panel.brands.index')}}"><i class="fa fa-cogs"></i> <span>Brand</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(2)=='merchants'?'active':''}}"><a href="{{route('control-panel.merchants.index')}}"><i class="fa fa-users"></i> <span>Merchant</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(2)=='product-categories'?'active':''}}"><a href="{{route('control-panel.product-categories.index')}}"><i class="fa fa-cogs"></i> <span>Product Category</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(2)=='product-subscriptions'?'active':''}}"><a href="{{route('control-panel.product-subscriptions.index')}}"><i class="fa fa-cogs"></i> <span>Product Subscription</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(2)=='subscription-plans'?'active':''}}"><a href="{{route('control-panel.subscription-plans.index')}}"><i class="fa fa-cogs"></i> <span>Subscription Plan</span></a></li>--}}
            {{--@endif--}}

            {{--<li class="has-sub"><a href="{{route('home.index')}}" target="_blank"><i class="fa fa-globe"></i> <span>Go To Website</span></a></li>--}}

            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
