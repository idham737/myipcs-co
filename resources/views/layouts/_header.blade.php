<div id="header" class="header navbar-default">
    <!-- begin navbar-header -->
    <div class="navbar-header">
        <a href="{{route('dashboard.index')}}" class="navbar-brand"> <img src="{!! asset('images/logo.png') !!}" width="70%" style="padding: 5px" alt="" /></a>
        <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- end navbar-header -->

    <!-- begin header navigation right -->
    <ul class="navbar-nav navbar-right">
        {{--<li>--}}
            {{--<form class="navbar-form full-width">--}}
                {{--<div class="form-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Enter keyword" />--}}
                    {{--<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>--}}
                {{--</div>--}}
            {{--</form>--}}
        {{--</li>--}}
        {{--<li class="dropdown">--}}
            {{--<a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">--}}
                {{--<i class="fa fa-bell"></i>--}}
                {{--<span class="label">5</span>--}}
            {{--</a>--}}
            {{--<ul class="dropdown-menu media-list pull-right">--}}
                {{--<li class="dropdown-header">NOTIFICATIONS (5)</li>--}}
                {{--<li class="media">--}}
                    {{--<a href="javascript:;">--}}
                        {{--<div class="media-left">--}}
                            {{--<i class="fa fa-bug media-object bg-silver-darker"></i>--}}
                        {{--</div>--}}
                        {{--<div class="media-body">--}}
                            {{--<h6 class="media-heading">Server Error Reports <i class="fa fa-exclamation-circle text-danger"></i></h6>--}}
                            {{--<div class="text-muted f-s-11">3 minutes ago</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="media">--}}
                    {{--<a href="javascript:;">--}}
                        {{--<div class="media-left">--}}
                            {{--<img src="../assets/img/user/user-1.jpg" class="media-object" alt="" />--}}
                            {{--<i class="fab fa-facebook-messenger text-primary media-object-icon"></i>--}}
                        {{--</div>--}}
                        {{--<div class="media-body">--}}
                            {{--<h6 class="media-heading">John Smith</h6>--}}
                            {{--<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>--}}
                            {{--<div class="text-muted f-s-11">25 minutes ago</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="media">--}}
                    {{--<a href="javascript:;">--}}
                        {{--<div class="media-left">--}}
                            {{--<img src="../assets/img/user/user-2.jpg" class="media-object" alt="" />--}}
                            {{--<i class="fab fa-facebook-messenger text-primary media-object-icon"></i>--}}
                        {{--</div>--}}
                        {{--<div class="media-body">--}}
                            {{--<h6 class="media-heading">Olivia</h6>--}}
                            {{--<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>--}}
                            {{--<div class="text-muted f-s-11">35 minutes ago</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="media">--}}
                    {{--<a href="javascript:;">--}}
                        {{--<div class="media-left">--}}
                            {{--<i class="fa fa-plus media-object bg-silver-darker"></i>--}}
                        {{--</div>--}}
                        {{--<div class="media-body">--}}
                            {{--<h6 class="media-heading"> New User Registered</h6>--}}
                            {{--<div class="text-muted f-s-11">1 hour ago</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="media">--}}
                    {{--<a href="javascript:;">--}}
                        {{--<div class="media-left">--}}
                            {{--<i class="fa fa-envelope media-object bg-silver-darker"></i>--}}
                            {{--<i class="fab fa-google text-warning media-object-icon f-s-14"></i>--}}
                        {{--</div>--}}
                        {{--<div class="media-body">--}}
                            {{--<h6 class="media-heading"> New Email From John</h6>--}}
                            {{--<div class="text-muted f-s-11">2 hour ago</div>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="dropdown-footer text-center">--}}
                    {{--<a href="javascript:;">View more</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        <li class="dropdown navbar-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                <img src="http://via.placeholder.com/50x50" alt="" />
                {{-- <span class="hidden-xs">{{auth()->user()->name}}</span> <b class="caret"></b> --}}
            </a>
            <ul class="dropdown-menu">
                <li class="arrow"></li>
                {{--<li><a href="javascript:;">Edit Profile</a></li>--}}
                {{--<li><a href="javascript:;"><span class="badge badge-danger pull-right">2</span> Inbox</a></li>--}}
                {{--<li><a href="javascript:;">Calendar</a></li>--}}
                {{--<li><a href="javascript:;">Setting</a></li>--}}
                <li class="divider"></li>
                <li><a href="{{route('logout')}}">Log Keluar</a></li>
            </ul>
        </li>
    </ul>
    <!-- end header navigation right -->
</div>
