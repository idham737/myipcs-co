<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>{!! env('APP_NAME') !!} | Login Page</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    @include('layout.head')
</head>
<body class="pace-top">
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="">
    <!-- begin login -->
    <div class="login login-with-news-feed">
        <!-- begin news-feed -->
        <div class="news-feed">
            <div class="news-image">
                <img src="{!! asset('images/logo-symbol.png') !!}" style="width: 40%;padding: 100px" data-id="" alt="" />
            </div>
            {{--<div class="news-caption">--}}
            {{--<h4 class="caption-title"> Monash University</h4>--}}
            {{--<p>--}}
            {{--Adopt as your fundamental creed that you will equip yourself for life, not solely for your own benefit but for the benefit of the whole community--}}
            {{--</p>--}}
            {{--</div>--}}
        </div>
        <!-- end news-feed -->
        <!-- begin right-content -->
        <div class="right-content">
            <!-- begin login-header -->
            <div class="login-header">
                <div class="brand">
                    MALAYSA INTEGRATED POPULATION CENSUS SYSTEM
                </div>
                <div class="icon">
                    <i class="fa fa-sign-in"></i>
                </div>
            </div>
            <!-- end login-header -->
            <!-- begin login-content -->
            <div class="login-content">
                @include('flash::message')

                <form action="{{ route('login') }}" method="POST" class="margin-bottom-0">
                    {{ csrf_field() }}

                    <div class="form-group m-b-15">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i> </span>
                            <input name="username" type="text" class="form-control input-lg"  value="{{ old('email') }}" placeholder="Username" />

                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block text text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group m-b-15">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                            <input name="password" type="password" class="form-control input-lg"  value="{{ old('password') }}" placeholder="Password" />

                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block text text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="login-buttons">
                        <button type="submit" class="btn btn-primary btn-block btn-lg">Sign me in</button>
                    </div>
                    {{--<div class="form-group m-b-15">--}}
                    {{--<input type="text" class="form-control input-lg" placeholder="Email Address" required />--}}
                    {{--</div>--}}
                    {{--<div class="form-group m-b-15">--}}
                    {{--<input type="password" class="form-control input-lg" placeholder="Password" required />--}}
                    {{--</div>--}}
                    {{--<div class="checkbox m-b-30">--}}
                    {{--<label>--}}
                    {{--<input type="checkbox" /> Remember Me--}}
                    {{--</label>--}}
                    {{--</div>--}}
                    {{--<div class="login-buttons">--}}
                    {{--<button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>--}}
                    {{--</div>--}}
                    {{--<div class="m-t-20 m-b-40 p-b-40 text-inverse">--}}
                    {{--Not a member yet? Click <a href="register_v3.html" class="text-success">here</a> to register.--}}
                    {{--</div>--}}
                    {{--<hr />--}}
                    {{--<p class="text-center">--}}
                    {{--&copy; Color Admin All Right Reserved 2015--}}
                    {{--</p>--}}
                </form>
            </div>
            <!-- end login-content -->
        </div>
        <!-- end right-container -->
    </div>
    <!-- end login -->

    <!-- begin theme-panel -->

    <!-- end theme-panel -->
</div>
{{--<div id="page-container" class="fade">--}}
{{--<!-- begin login -->--}}
{{--<div class="login bg-black animated fadeInDown">--}}
{{--<!-- begin brand -->--}}
{{--<div class="login-header">--}}
{{--<div class="brand">--}}

{{--</div>--}}
{{--<div class="icon">--}}
{{--<i class="fa fa-sign-in"></i>--}}
{{--</div>--}}
{{--</div>--}}
{{--<!-- end brand -->--}}
{{--<div class="login-content">--}}
{{--@include('flash::message')--}}

{{--<form action="{{ route('login') }}" method="POST" class="margin-bottom-0">--}}
{{--{{ csrf_field() }}--}}

{{--@if(env('DEVELOPMENT'))--}}

{{--<div class="form-group m-b-20">--}}
{{--<input name="username" type="text" class="form-control input-lg inverse-mode no-border"  value="{{ old('username') }}" placeholder="Username" />--}}
{{--@if ($errors->has('username'))--}}
{{--<span class="help-block text text-danger">--}}
{{--<strong>{{ $errors->first('username') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}

{{--@endif--}}
{{--<div class="login-buttons">--}}
{{--<button type="submit" class="btn btn-primary btn-block btn-lg">Sign me in</button>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--<!-- end login -->--}}

{{--<!-- begin theme-panel -->--}}

{{--<!-- end theme-panel -->--}}
{{--</div>--}}
<!-- end page container -->
@include('layout.script')
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
</body>
</html>
