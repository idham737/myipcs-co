@push('_styles')
@endpush

@push('_scripts')
    <script>

        window.pageNeedPermissionToLeave=false;

        $(document).ready(function(){

            $(window).on('beforeunload', function(){

                if( window.pageNeedPermissionToLeave){

                    return confirm('Leave page, unsaved data will be lost..');


                }else{
                    disableAllButton();
                }

                return null;


            });

        });


        function enableAllButton(){
            $('button').each(function(item){
                $(this).removeClass('disabled');
                $(this).removeAttr('disabled');
            });

            $('a').each(function(item){
                $(this).on('click');
                $(this).removeAttr("disabled");
            });
        }
        function disableAllButton(){
            $('button').each(function(item){
                $(this).addClass('disabled');
                $(this).attr('disabled','disabled');
            });



            $('a').each(function(item){
                $(this).off('click');
                $(this).attr("disabled", "disabled");
                e.preventDefault();
            });
        }
    </script>


    @if(session()->has('modal'))
        <script>
            $(document).ready(function(){
                $('{{session()->get('modal')}}').modal('show');
            });
        </script>


    @endif

    <script>
        $(document).ready(function(){
            $('form.remove-no-validate').each(function(item){
                $(this).removeAttr('novalidate');
            });
        });
    </script>
@endpush
