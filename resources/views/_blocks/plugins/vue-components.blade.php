
@push('_scripts')
    <script>
        Vue.component('date-picker', {
            template: '<input/>',
            props: [ 'dateFormat' ],
            mounted: function() {
                $(this.$el).datepicker({
                    dateFormat: this.dateFormat
                });
            },
            beforeDestroy: function() {  }
        });
    </script>
@endpush