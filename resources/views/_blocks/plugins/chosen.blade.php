@push('_styles')
    <link href="{{asset('assets/plugins/chosen_v1.8.7/chosen.css',config('core.https'))}}" rel="stylesheet" />
@endpush

@push('_scripts')
    <script src="{{asset('assets/plugins/chosen_v1.8.7/chosen.jquery.js',config('core.https'))}}"></script>
    <script>
        $(document).ready(function(){
            $(".chosen-select-modal").chosen({width:"100%"});
        })
    </script>
@endpush
