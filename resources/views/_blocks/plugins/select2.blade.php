@push('_styles')
    <link href="{{asset('plugins/select2-4.0.6-rc.1/dist/css/select2.min.css',config('core.https'))}}"
          rel="stylesheet"/>
    <link
        href="{{asset('plugins/select2-bootstrap4-theme-master/dist/select2-bootstrap4.min.css',config('core.https'))}}"
        rel="stylesheet"/>
    {{--<style>--}}
        {{--.select2-container--open {--}}
            {{--z-index: 99999999999 !important;--}}
        {{--}--}}
    {{--</style>--}}
@endpush

@push('_scripts')
    <script src="{{asset('plugins/select2-4.0.6-rc.1/dist/js/select2.full.min.js',config('core.https'))}}"></script>

    <script>
        $(document).ready(function () {
            $('.select2').select2({
                width: '100%',
                // theme: 'bootstrap4',
            });
            $('.select2-modal').select2({
                // minimumInputLength:3,
                // dropdownParent: $('.modal-body'),
                width: '100%',
                // theme: 'bootstrap4',
            });
        })
    </script>
@endpush
