@push('_styles')

@endpush

@push('_scripts')
    <script src="{{asset('plugins/sweetalert2/dist/sweetalert2.all.min.js',config('core.https'))}}"></script>
    @include('sweetalert::alert')
@endpush