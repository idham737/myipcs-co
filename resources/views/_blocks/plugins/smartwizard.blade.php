@push('_styles')
    <link href="{{asset('assets/plugins/jquery-smart-wizard/src/css/smart_wizard.css',config('core.https'))}}" rel="stylesheet" />
@endpush

@push('_scripts')
    <script src="{{asset('assets/plugins/jquery-smart-wizard/src/js/jquery.smartWizard.js',config('core.https'))}}"></script>
    <script>
        $(document).ready(function(){
            $('.wizard').smartWizard({
                selected: 0,
                theme: 'default',
                transitionEffect:'',
                transitionSpeed: 0,
                useURLhash: false,
                showStepURLhash: false,
                toolbarSettings: {
                    toolbarPosition: 'bottom'
                }
            });
        })
    </script>
@endpush