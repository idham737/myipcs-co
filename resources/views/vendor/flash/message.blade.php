@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else

        <div class="note note-{{ $message['level'] }}">
            <div class="note-icon"><i class="fa fa-exclamation"></i></div>
            <div class="note-content">
                <ul>
                    <li>{!! $message['message'] !!}</li>
                </ul>
            </div>
        </div>
        {{--<div class="alert--}}
                    {{--alert---}}
                    {{--{{ $message['important'] ? 'alert-important' : '' }}"--}}
                    {{--role="alert"--}}
        {{-->--}}
            {{--@if ($message['important'])--}}
                {{--<button type="button"--}}
                        {{--class="close"--}}
                        {{--data-dismiss="alert"--}}
                        {{--aria-hidden="true"--}}
                {{-->&times;</button>--}}
            {{--@endif--}}

            {{----}}
        {{--</div>--}}
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
