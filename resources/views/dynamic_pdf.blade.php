<!DOCTYPE html>
<html>
<head>
<title>Convert to PDF</title>
<style type="text/css">
    table{
        width: 100%;
        margin: 0 auto;
        border: 1px solid;
    }
</style>
<body>
    {{-- <a href="{{ route('getPDF') }}">Download<a> --}}
    <table>
        <caption><h1>Customer Info</h1></caption>
        <a href = "{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Convert to PDF</a>
        <thead>
            <tr>
                {{-- <th>No.</th> --}}
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Sex</th>
                <th>Email</th>
                <th>Phone</th>
            </tr>
        </thead>
        <tbody>
            @foreach($customer_data as $c)
            <tr>
                {{-- <td>{{ ++$key }}</td> --}}
                <td>{{$c->id}}</td>
                <td>{{$c->firstname}}</td>
                <td>{{$c->lastname}}</td>
                <td>{{ $c->sex }}</td>
                {{-- <td>
                    @if($c->sex==0)
                            Male
                    @else
                            Female
                    @endif
                </td> --}}
                <td>{{$c->email}}</td>
                <td>{{$c->phone}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
<body>
</head>
<html>