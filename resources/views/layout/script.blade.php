
<script src="{{asset('assets/plugins/jquery/jquery-3.2.1.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/jquery-ui.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js',config('core.https'))}}"></script>

<!-- ================== BEGIN BASE JS ================== -->
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js',config('core.https'))}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js',config('core.https'))}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/js-cookie/js.cookie.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/theme/default.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/apps.js',config('core.https'))}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/plugins/gritter/js/jquery.gritter.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.resize.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.pie.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/js/demo/dashboard.min.js',config('core.https'))}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->


<script src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js',config('core.https'))}}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/masked-input/masked-input.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/password-indicator/js/password-indicator.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-combobox/js/bootstrap-combobox.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-tag-it/js/tag-it.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/moment.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/select2/dist/js/select2.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-show-password/bootstrap-show-password.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js',config('core.https'))}}"></script>
<script src="{{asset('assets/plugins/clipboard/clipboard.min.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/fullcalendar-3.9.0/fullcalendar.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/Highcharts-6.1.1/code/js/highcharts.js',config('core.https'))}}"></script>
<script src="{{asset('plugins/Highcharts-6.1.1/code/js/highcharts-3d.js',config('core.https'))}}"></script>
{{--<script src="{{asset('assets/js/form-plugins.demo.min.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.esm.js',config('core.https'))}}"></script>--}}
{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.common.js',config('core.https'))}}"></script>--}}

{{--<script src="{{asset('plugins/vuejs-datepicker/dist/vuejs-datepicker.common.js',config('core.https'))}}"></script>--}}

<script src="{{asset('plugins/vuejs-datepicker/dist/vuejs-datepicker.min.js',config('core.https'))}}"></script>
<script src="{{asset('js/moment.js',config('core.https'))}}"></script>
<script src="{{asset('js/vue.js',config('core.https'))}}"></script>


<!-- ================== END PAGE LEVEL JS ================== -->
<!-- Data tables -->
<script>
    $.fn.datepicker.defaults.autoclose = true;
</script>
<script>


    $(document).ready(function() {


        App.init();
        // Dashboard.init();
        // FormPlugins.init();
    });
</script>

@if(session()->has('snackbar'))
    <script type="text/javascript">
        $(document).ready(function ()
        {
            new PNotify({
                text    : '{{session('snackbar')}}',
                confirm : {
                    confirm: true,
                    buttons: [
                        {
                            text    : 'Dismiss',
                            addClass: 'btn btn-link',
                            click   : function (notice)
                            {
                                notice.remove();
                            }
                        },
                        null
                    ]
                },
                buttons : {
                    closer : false,
                    sticker: false
                },
                animate : {
                    animate  : true,
                    in_class : 'slideInDown',
                    out_class: 'slideOutUp'
                },
                addclass: 'md'
            });

        });

    </script>
@endif

<script>
    (function ()
    {
        $(document).ready(function ()
        {
            $('.datatable-layout-2').DataTable(
                {
                    dom         : 'Brtip',
                    initComplete: function ()
                    {
                        var api = this.api(),
                            searchBox = $('#datatable-layout-2-search-input');

                        // Bind an external input as a table wide search box
                        if ( searchBox.length > 0 )
                        {
                            searchBox.on('keyup', function (event)
                            {
                                api.search(event.target.value).draw();
                            });
                        }
                    },
                    lengthMenu  : [10, 20, 30, 50, 100],
                    pageLength  : 10,
                    scrollY     : 'auto',
                    scrollX     : false,
                    responsive  : true,
                    autoWidth   : false,
                    buttons: [
                        'copy', 'excel', 'pdf'
                    ],
                    "order": []
                }
            );

            $('.datatable-showall').DataTable({
                "aLengthMenu": [[-1], ["All"]],
                "bFilter": true,
                responsive: true,
                "order": [],
            });

            $('.datatable').DataTable({
                // "bFilter": true,
                responsive: true,
                "order": [],
            });

            $('.datatable-exportable').DataTable({
                // "bFilter": true,
                "dom": '<B>T<"clear">lfrtip',
                responsive: true,
                buttons: [
                    'csv', 'excel', 'pdf'
                ],
                "order": []
            });


        });
    })();
</script>
@if(session()->has('modal'))
    <script type="text/javascript">
        $(document).ready(function ()
        {
            $('#{{session('modal')}}').modal('show');

        });

    </script>
@endif

@if(session()->has('openTab'))
    <script type="text/javascript">
        $(document).ready(function ()
        {
            {{--$('{{session('openTab')}}').tab('show');--}}
                    console.log('opentab','{{session('openTab')}}');
            $('a[href="#{{session('openTab')}}"]').tab('show')
        });

    </script>
@endif

<script>
    $('#flash-overlay-modal').modal();
</script>


<script>
    $(document).ready(function(){
        $('.datetimepicker').datetimepicker({
            format: 'DD/MM/YYYY hh:mm A'
        });

        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });



        $('.datetimepickerstart').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY hh:mm A'
        });
        $('.datetimepickerend').datetimepicker({
            useCurrent: false,
            format: 'DD/MM/YYYY hh:mm A'
        });
        $(".datetimepickerstart").on("dp.change", function (e) {
            $('.datetimepickerend').data("DateTimePicker").minDate(e.date);
        });
    })
</script>
