<meta charset="utf-8" />
<title>Monash University</title>
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />


<!-- ================== BEGIN BASE CSS STYLE ================== -->
<link href="{{config('core.https')?'https':'http'}}://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link href="{{asset('assets/plugins/jquery-ui/jquery-ui.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
{{--<link href="{{asset('assets/plugins/font-awesome/5.0/css/fontawesome.css',config('core.https'))}}" rel="stylesheet" />--}}
<link href="{{asset('assets/plugins/font-awesome/5.0/css/fontawesome-all.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/animate/animate.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/css/default/style.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/css/default/style-responsive.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/css/default/theme/blue.css',config('core.https'))}}" rel="stylesheet" id="theme" />
<!-- ================== END BASE CSS STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/gritter/css/jquery.gritter.css',config('core.https'))}}" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('assets/plugins/pace/pace.min.js',config('core.https'))}}"></script>
<!-- ================== END BASE JS ================== -->


<link href="{{asset('assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',config('core.https'))}}" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/ionRangeSlider/css/ion.rangeSlider.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/password-indicator/css/password-indicator.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-combobox/css/bootstrap-combobox.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-select/bootstrap-select.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/jquery-tag-it/css/jquery.tagit.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/select2/dist/css/select2.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-fontawesome.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker-glyphicons.css',config('core.https'))}}" rel="stylesheet" />

<link href="{{asset('plugins/fullcalendar-3.9.0/fullcalendar.css',config('core.https'))}}" rel="stylesheet" />
<link href="{{asset('plugins/Highcharts-6.1.1/code/css/highcharts.css',config('core.https'))}}" rel="stylesheet" />


{{--<link href="{{asset('plugins/bootstrap-vue/dist/bootstrap-vue.css',config('core.https'))}}" rel="stylesheet" />--}}
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN BASE JS ================== -->
<!-- ================== END BASE JS ================== -->

<style>
    div.required .control-label:after,label.required:after {
        content:"*";
        color:red;
    }
    .reset-a, .reset-a:hover, .reset-a:visited, .reset-a:focus, .reset-a:active  {
        text-decoration: none;
        color: inherit;
        outline: 0;
        cursor: auto;
    }

    .bigger-form-font{
        font-size: 14px !important;
    }
</style>