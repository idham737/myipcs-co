<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    @include('layout.head')
    <style>
        div.dt-buttons {
            float: right;
        }
        .invalid-feedback {
            display: block;
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
        }



    </style>


</head>

<body>

<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
@if(in_array(request()->segment(1),['requests','calendar-events']))

    <style>
        .page-content-full-height .content {
            position: absolute;
            left: 0;
            top: 90px;
            right: 0;
            bottom: 0;
            -webkit-transform: translateZ(0);
            transform: translateZ(0);
        }
    </style>

        <div id="page-container" class="page-container page-without-sidebar page-header-fixed page-with-top-menu page-content-full-height">

@else
            <div id="page-container" class="page-container page-without-sidebar page-header-fixed page-with-top-menu">

            @endif

    <!-- begin #header -->
    <div id="header" class="header navbar-default">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand" >
                <img src="{{asset('images/monash-logo-mono.svg',config('core.https'))}}" width="70" style="display: inline-block !important;">
                {{(new \App\FormRequest())->getAppName()}}</a>
            <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->

        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">

            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    @if(auth()->user()->profile_picture)
                        <img src="{{asset('images/profile/'.auth()->user()->profile_picture,config('core.https'))}}" alt="" />

                    @else
                        <img src="{{asset('images/profile/placeholder.png',config('core.https'))}}" alt="" />


                    @endif
                    <span class="hidden-xs">{{strtoupper(auth()->user()->name)}}</span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="{{route('my_account.index')}}">My Account</a></li>
                    <li><a href="{{route('logout')}}">Log Out</a></li>
                </ul>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <div id="top-menu" class="top-menu">
        <!-- begin sidebar user -->
        {{--<ul class="nav">--}}
            {{--<li class="nav-profile">--}}
                {{--<div class="image">--}}
                    {{--@if(auth()->user()->profile_picture)--}}
                        {{--<a href="javascript:;"><img src="{{asset('images/profile/'.auth()->user()->profile_picture,config('core.https'))}}" alt="" /></a>--}}

                    {{--@else--}}
                        {{--<a href="javascript:;"><img src="{{asset('images/profile/placeholder.png',config('core.https'))}}" alt="" /></a>--}}

                    {{--@endif--}}
                {{--</div>--}}
                {{--<div class="info">--}}
                    {{--{{auth()->user()->name}}--}}
                    {{--<small>{{implode(',',auth()->user()->roles()->pluck('name')->toArray())}}</small>--}}
                {{--</div>--}}
            {{--</li>--}}
        {{--</ul>--}}
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            {{--<li class="nav-header">Navigation</li>--}}
            <li class="has-sub {{request()->segment(1)=='home'||request()->segment(1)==null?'active':''}}"><a href="{{route('home.index')}}"><i class="fa fa-home"></i> <span>Home</span></a></li>

            @if(auth()->user()->show_statistic_data)
            <li class="has-sub {{request()->segment(1)=='dashboard'?'active':''}}"><a href="{{route('dashboard.index')}}"><i class="fa fa-chart-pie"></i> <span>Dashboard</span></a></li>
            @endif

            @if(auth()->user()->hasRole('requester'))
                <li class="has-sub {{request()->segment(1)=='requests'&&request()->segment(2)=='create'?'active':''}}"><a href="{{route('requests.create')}}"><i class="fa fa-file-alt"></i> <span>New Request</span></a></li>

                {{--                <li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('my-request')?'active':''}}"><a href="{{route('requests.index',['my-request'])}}"> <i class="fa fa-list"></i><span>My Request</span><span class="badge pull-right">{{$myRequestCount}}</span></a></li>--}}
{{--                <li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('show-completed')?'active':''}}"><a href="{{route('requests.index',['show-completed'])}}"><i class="fa fa-list"></i><span>Completed Request</span><span class="badge pull-right">{{$completedCount}}</span></a></li>--}}
                {{--<li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('show-all')?'active':''}}"><a href="{{route('requests.index',['show-all'])}}"><i class="fa fa-list"></i><span>All Request</span></a></li>--}}
            @endif
            @if(auth()->user()->hasRole(['verifier','approver','admin','security admin','catering admin']))
                {{--<li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('in-progress')?'active':''}}"><a href="{{route('requests.index',['in-progress'])}}"><i class="fa fa-list"></i><span>Request In Progress</span><span class="badge pull-right">{{$inProgressCount}}</span></a></li>--}}
{{--                <li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('show-completed')?'active':''}}"><a href="{{route('requests.index',['show-completed'])}}"><i class="fa fa-list"></i><span>Completed Request</span><span class="badge pull-right">{{$completedCount}}</span></a></li>--}}
{{--                <li class="has-sub {{request()->segment(1)=='requests'&&\Request::exists('show-all')?'active':''}}"><a href="{{route('requests.index',['show-all'])}}"><i class="fa fa-list"></i><span>All Request</span></a></li>--}}
            @endif

            <li class="has-sub {{request()->segment(1)=='requests'&&request()->segment(2)!='create'?'active':''}}"><a href="{{route('requests.index')}}"><i class="fa fa-list"></i> <span>Requests</span></a></li>
            @if(isGroup('ems'))
            <li class="has-sub {{request()->segment(1)=='calendar-events'}}"><a href="{{route('calendar-events.index',['published-event'])}}"><i class="fa fa-calendar"></i> <span>Calendar Event</span></a></li>

        @endif

        @if(auth()->user()->hasRole('admin'))

                <li class="has-sub  {{request()->segment(1)=='management'?'active':''}}">
                    <a href="javascript:;">
                        <b class="caret pull-right"></b>
                        <i class="fa fa-cogs"></i>
                        <span>Management</span>
                    </a>
                    <ul class="sub-menu">
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='departments'?'active':''}}"><a href="{{route('management.departments.index')}}">Department</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='positions'?'active':''}}"><a href="{{route('management.positions.index')}}">Position</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='users'&&request()->input('role')=='admin'?'active':''}}"><a href="{{route('management.users.index',['role'=>'admin'])}}">Admin</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='users'&&request()->input('role')=='approver'?'active':''}}"><a href="{{route('management.users.index',['role'=>'approver'])}}">Approver</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='users'&&request()->input('role')=='verifier'?'active':''}}"><a href="{{route('management.users.index',['role'=>'verifier'])}}">Verifier</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='users'&&request()->input('role')=='requester'?'active':''}}"><a href="{{route('management.users.index',['role'=>'requester'])}}">Requester</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='users'&&!request()->has('role')?'active':''}}"><a href="{{route('management.users.index')}}">All User</a></li>
                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='email-templates'?'active':''}}"><a href="{{route('management.email-templates.index')}}">Email Template</a></li>
                        @if($formRequestGroup=='marketingMerchandise')
                            <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='merchandise-items'?'active':''}}"><a href="{{route('management.merchandise-items.index')}}">Merchandise Item</a></li>
                        @endif
                        @if($formRequestGroup=='ems')
                            <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='caterers'?'active':''}}"><a href="{{route('management.caterers.index')}}">Caterer</a></li>
                        @endif
                        {{--                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='layout-plans'?'active':''}}"><a href="{{route('management.layout-plansout-plans.index')}}">Layout Plan</a></li>--}}
                        {{--                        <li class="{{request()->segment(1)=='management'&&request()->segment(2)=='branches'?'active':''}}"><a href="{{route('management.branches.index')}}">Branch</a></li>--}}
                    </ul>
                </li>
                {{--<li class="has-sub {{request()->segment(1)=='report'?'active':''}}">--}}
                {{--<a href="javascript:;">--}}
                {{--<b class="caret pull-right"></b>--}}
                {{--<i class="fa fa-cogs"></i>--}}
                {{--<span>Report</span>--}}
                {{--</a>--}}
                {{--<ul class="sub-menu">--}}
                {{--<li class="{{request()->segment(1)=='report'&&request()->segment(2)=='attendance'?'active':''}}"><a href="{{route('report.attendance.index',['date'=>\Carbon\Carbon::now()->format('Y-m-d')])}}">Attendance</a></li>--}}
                {{--</ul>--}}
                {{--</li>--}}
            @endif
{{--            <li class="has-sub {{request()->segment(1)=='my-account'?'active':''}}"><a href="{{route('my_account.index')}}"><i class="fa fa-user"></i> <span>My Account</span></a></li>--}}

            {{--<li class="has-sub {{request()->segment(1)=='logout'?'active':''}}"><a href="{{route('logout')}}"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>--}}


            {{--@foreach(['ems_demo_requester_unit','ems_demo_verifier_unit','ems_demo_verifier2','approver_media','ems_demo_approver','approver_ohse','ems_demo_security_admin','ems_demo_catering_admin'] as $username)--}}
            {{--<li style="font-size: 8px" class="has-sub {{request()->segment(1)==null?'active':''}}"><a href="{{url('development/login',['username'=>$username])}}"><i class="fa fa-user"></i> <span> Login as {{$username}}</span></a></li>--}}

            {{--@endforeach--}}
            {{--<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>--}}
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    {{--<div class="sidebar-bg"></div>--}}
    @section('content2')
        @if(in_array(request()->segment(1),['requests','calendar-events']))
                        <div id="content" class="content content-full-width inbox">
            @else
                    <div id="content" class="content">
                        {{ \Breadcrumbs::render() }}
                        {{--<ol class="breadcrumb pull-right">--}}
                        {{--{!! \Breadcrumbs::render() !!}--}}

                        {{--</ol>--}}
                        @include('flash::message')

                        @endif

        @yield('content')
    </div>
    @show
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end page container -->
@include('layout.script')


@yield('js')

                        <script>
                            $(document).ready(function(){
                                var handleSlimScroll2 = function() {
                                    "use strict";
                                    $('.custom-scroll').each( function() {
                                        generateSlimScroll2($(this));
                                    });
                                };
                                var generateSlimScroll2= function(element) {
                                    if ($(element).attr('data-init')) {
                                        return;
                                    }
                                    var dataHeight = $(element).attr('data-height');
                                    dataHeight = (!dataHeight) ? $(element).height() : dataHeight;

                                    var scrollBarOption = {
                                        height: dataHeight,
                                        size: '100px',
                                        alwaysVisible: true,
                                        railVisible: true,
                                        disableFadeOut: true
                                    };
                                    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                        $(element).css('height', dataHeight);
                                        $(element).css('overflow-x','scroll');
                                    } else {
                                        $(element).slimScroll(scrollBarOption);
                                    }
                                    $(element).attr('data-init', true);
                                    $('.slimScrollBar').hide();
                                };

                                handleSlimScroll2();
                            })


                        </script>

</body>
</html>
