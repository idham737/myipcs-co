@extends('layouts.page_with_sidebar.main')
@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
                <div class="row" >
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <i class="fa fa-list"></i>
                                Serah Tugas
                                <div class="panel-heading-btn">
                                    <form class="form-inline pull-right">
                                        <div class="form-group justify-content-center">
                                            <label for="" style="color:white"> Lingkungan Banci &nbsp;</label>
                                            <input type="text" class="form-control text-center" value="01" style="height:20px">
                                        </div>
                                    </form>
                                    {{--@role('admin')--}}
                                    {{--<a href="{{route($baseRoute.'.create')}}" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> {{$resourceName}}</a>--}}
                                    {{--@endrole--}}

                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group col-md-12" > 
                        <h3 class="card-header">Serah Tugas
                            <form class="form-inline pull-right">
                                <div class="form-group">
                                    <label for=""><h5 style="color:blue;"> Lingkungan Banci &nbsp;</h5></label>
                                    <input type="text" class="form-control text-center" value="01">
                                </div>
                            </form>
                        </h3>
                    </div> --}}
                </div>
                          
              <div class="form-group row" >
                <label class="col-md-10 col-form-label" for="password-input"><p style="color:blue;">Peta Blok Penghitungan</p>
                  <p style="color:black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sila pilih Lihat Blok Penghitungan untuk serah tugas kepada Pembanci</p></label>
                <div class="col-md-2">
                  <div class="col-12 text-center">
                    <button class="btn btn-info" style="color:aliceblue;">Lihat</button>
                </div>
                </div>
              </div>
              <div class="form-group row" >
                <div class="col-md-7">
                  <div class="container">
                      <table width="100%" class="datatable table table-striped">
                        <thead>
                        <tr>
                            <td width="1%">No.</td>
                            <td>Blok Penghitungan</td>
                            <td>Pembanci bertugas</td>
                            <td>Jumlah TK</td>
                            <td>Tarikh Serah</td>
                            <td>Tindakan</td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>001</td>
                                <td>-</td>
                                <td>Jumlah</td>
                                <td>-</td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>002</td>
                                <td>Suraya</td>
                                <td>120</td>
                                <td>1/8/2018</td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>003</td>
                                <td>-</td>
                                <td>Jumlah</td>
                                <td>-</td>
                                <td align="right">
                                    <div style="white-space: nowrap">
                                        <a href="" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-5">
                <div class="col-12 text-center">   
                    <img src="images/gerikplang.png" class="rounded" alt="Cinque Terre" style="width:100%" class="center"> 
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection