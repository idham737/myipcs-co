@extends('layouts.page_with_sidebar.main')
     @section('content')
     <style>
            .button7 {
                background-color: #4CAF50; /* Green */
                padding: 6px 40px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 12px;
                margin: 4px 12px;
                cursor: pointer;
            }
    </style>  

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                        <div class="card-body">
                                <div class="row">
                                        <div class="col-sm-6 col-md-3">
                                            <div class="card-header">                       
                                              <div class="card-body">
                                                   
                                                    <b>Nama :</b>
                                                    <br>
                                                    Nur Amalina binti Ahmad  
                                                    <br><br>
                                                   <b>Alamat :</b> 
                                                   <br>
                                                   E-3-01 , Rumah Pangsa Damai . Jalan Tropicana Selatan
                                                  
                                                      
                                                </div>
                                            </div>
                                            <br>
                                            <a class="btn btn-block btn-default" type="button"> Alamat Lengkap</a> 
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Tempat Kediaman (TK)</a>
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Isi Rumah (IR)</a> 
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Perseorangan</a> 
                                            <a class="btn btn-block btn-default" type="button"> Tetapan Temujanji</a>  

                                        </div>
                                        
                                        
                                        <div class="col-sm-6 col-md-7">
                                                    <div class="container-fluid">
                                                        <p>Sila periksa soalan dan jawapan untuk memastikan jawapan yang diberikan lengkap dan sama. Setelah selesai tekan butang "Pinda Semula"</p>

                                                            <div class="panel panel-inverse">
                                                                <div class="panel-heading">
                                                                    <i class="fa fa-list"></i>
                                                                    Butir-Butir Perseorangan
                                                                </div>
                                                            </div>

                                                            <table width="100%" class="datatable table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <td width="1%">No.</td>
                                                                        <td>Nama</td>
                                                                        <td width="1%">Status</td>
                                                                        <td width="1%">Tindakan</td>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td>Abu</td>
                                                                            <td>Belum</td>
                                                                            <td align="center">
                                                                                <div style="white-space: nowrap">
                                                                                    <a href="{{ route('personal2') }}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2</td>
                                                                            <td>Dzulfikar</td>
                                                                            <td>Belum</td>
                                                                            <td align="center">
                                                                                <div style="white-space: nowrap">
                                                                                    <a href="{{ route('personal2') }}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>3</td>
                                                                            <td>Karimah Kadir</td>
                                                                            <td>Selesai</td>
                                                                            <td align="center">
                                                                                <div style="white-space: nowrap">
                                                                                    <a href="{{ route('personal2') }}" class="btn btn-info btn-xs" title="Perincian"><i class="fa fa-folder"></i></a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                    </div>
                                                    </div>
                                        
                                                    <div class="col-sm-6 col-md-2">
                                                        <br><br>
                                                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                                                            <a href="">
                                                            <button class="btn btn-block btn-success" style="background-color:green" type="button"><i class="fa fa-edit"></i> Kemaskini</button>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                                                            <a href="">
                                                                <button class="btn btn-block btn-success"style="background-color:green" type="button"><i class="fa fa-check"></i> Sah</button>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        
                                                    </div>
                                                    
                                                  
                                </div>
                            
                        </div>
                    </div>
                  
                </div>
            </div>

        </div>
    </div>
    <!-- /.row-->
</div>
@endsection