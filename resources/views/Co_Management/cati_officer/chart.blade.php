@extends('layouts.core2')

@push('_styles')
    @stack('styles')
@endpush

@push('_scripts')
    @stack('scripts')
@endpush

@section('main_content')
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed show">
        <!-- begin #header -->
        @include('layouts._header')
        @include('layouts._sidebar')
        @hasSection('content')
            <div id="content" class="content vue-app-content">
            @yield('content')
            </div>
    @endif
        <!-- end #content -->

        <!-- begin theme-panel -->

        <!-- end theme-panel -->

        <!-- begin scroll to top btn -->
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        <!-- end scroll to top btn -->
    </div>



<div class="container">
    <div class="panel panel-inverse">
        <div class="panel-heading" style="text-color:white">
                <i class="fa fa-list"></i>
                Carta
            </div>
    
    <div class="panel-body">
        <div class="container">
            <div>
                {!! $chart->html() !!}
            </div>
        </div>
    </div>
        </div>
</div>

        <!-- End Of Main Application -->

        {!! Charts::scripts() !!}

        {!! $chart->script() !!}

    {{-- </body> --}}

@endsection