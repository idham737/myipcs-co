@extends('layouts.page_with_sidebar.main')
     @section('content')
     <style>
            .button7 {
                background-color: #4CAF50; /* Green */
                padding: 6px 40px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 12px;
                margin: 4px 12px;
                cursor: pointer;
            }
    </style>  

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                        <div class="card-body">
                                <div class="row">
                                        <div class="col-sm-6 col-md-3">
                                            <div class="card-header">                       
                                              <div class="card-body">
                                                   
                                                    <b>Nama :</b>
                                                    <br>
                                                    Nur Amalina binti Ahmad  
                                                    <br><br>
                                                   <b>Alamat :</b> 
                                                   <br>
                                                   E-3-01 , Rumah Pangsa Damai . Jalan Tropicana Selatan
                                                  
                                                      
                                                </div>
                                            </div>
                                            <br>
                                            <a class="btn btn-block btn-default" type="button"> Alamat Lengkap</a> 
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Tempat Kediaman (TK)</a>
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Isi Rumah (IR)</a> 
                                            <a class="btn btn-block btn-default" type="button"> Butir-butir Perseorangan</a> 
                                            <a class="btn btn-block btn-default" type="button"> Tetapan Temujanji</a>  

                                        </div>
                                        
                                        
                                        <div class="col-sm-6 col-md-7">
                                                    <div class="container-fluid">
                                                        <p>Sila periksa soalan dan jawapan untuk memastikan jawapan yang diberikan lengkap dan sama. Setelah selesai tekan butang "Pinda Semula"</p>

                                                            <div class="panel panel-inverse">
                                                                <div class="panel-heading">
                                                                    <i class="fa fa-list"></i>
                                                                        Butir-Butir Perseorangan
                                                                </div>
                                                            </div>

                                                            <form action="/action_page.php">
                                                                <div class="row">
                                                                <div class="form-group col-md-5">
                                                                    <label for="usr">D1. Nombor Pengenalan Diri <br>Jenis</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d1b">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">No. MyKad/Mykid/Mypr/MyKAS</option>
                                                                        <option value="2">No. K.P.Lama/ Tentera/Polis/Passport</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                        <label for="usr"><br>Nombor</label>
                                                                        <input type="text" class="form-control" id="usr" value="" name = "bair_d1a">
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-9">
                                                                        <label for="usr">D2. Nama Penuh </label>
                                                                        <input type="text" class="form-control" id="usr" value="" name = "bair_d2">
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-5">
                                                                    <label for="pwd">D3. Hubungan dengan Isi Rumah</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d3">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ketua isi Rumah</option>
                                                                        <option value="2">Isteri / Suami Ketua</option> 
                                                                        <option value="3">Anak Ketua yang belum berkahwin</option> 
                                                                        <option value="4">Anak Ketua yang sudah berkahwin</option> 
                                                                        <option value="5">Menantu perempuan / lelaki ketua</option> 
                                                                        <option value="6">Cucu ketua</option> 
                                                                        <option value="7">Bapa/ Ibu ketua atau kepada Isteri /Suami ketua</option> 
                                                                        <option value="8">Datuk/ nenek ketua atau kepada isteri/ Suami ketua</option> 
                                                                        <option value="9">Abang/ kakak/ adik ketua atau kepada isteri/ suami ketua</option> 
                                                                        <option value="10">Orang lain yang bersaudara dengan ketua atau isteri</option> 
                                                                        <option value="11">Pembantu Rumah</option> 
                                                                        <option value="12">Orang lain yang tidak bersaudara dengan ketua atau isteri ketua</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-3">
                                                                    <label for="pwd">D4. Jantina</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d4">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Lelaki</option>
                                                                        <option value="0">Perempuan</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-3">
                                                                    <label for="pwd">D5. Tarikh Lahir</label>
                                                                    <input type="date" class="form-control" id="usr" value="" name = "bair_d5">
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-2">
                                                                    <label for="pwd">D6. Umur - Tahun</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d6">
                                                                </div>
                                                                <div class="form-group col-md-3">
                                                                    <label for="pwd">D7. Taraf Perkahwinan</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d7">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Tidak pernah berkahwin</option>
                                                                        <option value="2">Berkahwin</option> 
                                                                        <option value="3">Balu/duda</option> 
                                                                        <option value="4">Bercerai</option> 
                                                                        <option value="5">Berpisah</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd">D8. Umur perkahwinan pertama -Tahun</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d8">
                                                                </div>
                                                                <div class="form-group col-md-3">
                                                                    <label for="pwd">D9. Kumpulan Etnik<br><br></label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d9">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">1100-Melayu</option>
                                                                        <option value="2">2111-Bateq</option> 
                                                                        <option value="3">2112-Jahal</option> 
                                                                        <option value="4">2113-Kensul</option> 
                                                                        <option value="5">2114-Kintak</option> 
                                                                        <option value="6">2115-Lanoh</option>
                                                                        <option value="7">2116-Mendriq</option> 
                                                                        <option value="8">2121-Che Wong</option> 
                                                                        <option value="9">2122-Jahut</option> 
                                                                        <option value="10">9889- lain-lain etnik</option> 
                                                                    </select>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d10lain">
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D10. Apakah agama / kepercayaan yang dianuti oleh anda?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d10">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Islam</option>
                                                                        <option value="2">Kristian</option> 
                                                                        <option value="3">Buddha</option> 
                                                                        <option value="4">Hindu</option> 
                                                                        <option value="5">Sikhism</option> 
                                                                        <option value="6">Tao</option>
                                                                        <option value="7">Konfusianisme</option> 
                                                                        <option value="8">Bahal</option> 
                                                                        <option value="9">Puak/ Suku/ Folk/ Agama tradisi lain orang cina</option> 
                                                                        <option value="10">Animisme</option> 
                                                                        <option value="11">Tiada Agama</option> 
                                                                        <option value="12">Lain-lain agama</option> 
                                                                    </select>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d10lain">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D11. Adakah sesiapa dalam Isi Rumah ini Orang Kurang Upaya?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d11">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya</option>
                                                                        <option value="0">Tidak</option> 
                                                                    </select>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d11a">
                                                                </div>
                                                                </div>
                                                                {{-- <div class="row">
                                                                
                                                                </div> --}}
                                                                <label for="pwd"><br><br>Kesuburan</label>
                                                                <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd">D12. <br>a)Pernahkah anda melahirkan anak?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d12a">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya</option>
                                                                        <option value="0">Tidak</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd"><br>b)Bilangan anak yang dilahirkan hidup?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d12b">
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd"><br>c)Berapakah daripadanya yang masih hidup sekarang?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d12c">
                                                                </div>
                                                                </div>
                                                                <label for="pwd"><br><br>Migrasi</label>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D13. Tempat Lahir</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d13">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Malaysia</option>
                                                                        <option value="0">Luar Malaysia</option> 
                                                                    </select>
                                                                    <label for="pwd">Negeri dilahirkan</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d13a">
                                                                    <label for="pwd">Negara dilahirkan</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d13b">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D14. Kewarganegaraan</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d14">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Warganegara Malaysia</option>
                                                                        <option value="2">Bukan Warganegara Malaysia</option> 
                                                                        <option value="3">Penduduk Tetap</option> 
                                                                        <option value="4">Ekspatriat (Pegawai dagang)</option> 
                                                                        <option value="5"}>Keluarga Ekspatriat</option> 
                                                                        <option value="6">Pelawat</option>
                                                                        <option value="7">Pelajar</option> 
                                                                        <option value="8">Pekerja asing (Selain daripada Ekspatriat)</option> 
                                                                        <option value="9">Lain-lain (Sila nyatakan)</option> 
                                                                    </select>
                                                                    <label for="pwd">Nama Negara(Jika lain-lain)</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d14a">
                                                                    <label for="pwd">Kod Negara</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d14b">
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D15. Tempat tinggal biasa 1 tahun yang lalu?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15">
                                                                    <label for="pwd">Nama Jalan/ Taman Perumahan</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15a">
                                                                    <label for="pwd">Nama Bandar/ Kampung</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15b">
                                                                    <label for="pwd">Mukim/ Daerah(Kelantan)/ Daerah Kecil(Sarawak)</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15c">
                                                                    <label for="pwd">Daerah Pentadbiran/ Jajahan(Kelantan)</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15d">
                                                                    <label for="pwd">Negeri/ Negara</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d15e">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D16. Tempat tinggal biasa 5 tahun yang lalu?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16">
                                                                    <label for="pwd">Nama Jalan/ Taman Perumahan</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16a">
                                                                    <label for="pwd">Nama Bandar/ Kampung</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16b">
                                                                    <label for="pwd">Mukim/ Daerah(Kelantan)/ Daerah Kecil(Sarawak)</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16c">
                                                                    <label for="pwd">Daerah Pentadbiran/ Jajahan(Kelantan)</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16d">
                                                                    <label for="pwd">Negeri/ Negara</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16e">
                                                                </div>
                                                                </div>
                                                                <label for="pwd"><br><br>Pendidikan</label><br>
                                                                <div class="row">
                                                                <div class="form-group col-md-8">
                                                                    <label for="pwd">D17. <br>a)Pernahkah anda bersekolah Rendah / Menengah / Maktab / Politeknik / Kolej / Universiti?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d17b">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya, masih belajar (Sepenuh masa)</option>
                                                                        <option value="1">Ya, masih belajar (Sambilan)</option>
                                                                        <option value="1">Ya, telah tamat pelajaran</option>
                                                                        <option value="1">Tidak, terlalu muda</option>
                                                                        <option value="0">Tidak, pernah bersekolah</option> 
                                                                    </select>
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd"><br>b)Bolehkah anda membaca dan menulis?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d16e">
                                                                </div>
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-7">
                                                                    <label for="pwd">D18. Peringkat pendidikan tertinggi yang dicapai / masih menuntut</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d18">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Pendidikan Prasekolah</option>
                                                                        <option value="2">Pendidikan Rendah</option> 
                                                                        <option value="3">Pendidikan Menengah Rendah</option> 
                                                                        <option value="4">Pendidikan Menengah Atas</option> 
                                                                        <option value="5">Pra Universiti</option> 
                                                                        <option value="6">Pendidikan Pasca menengah bukan Tertiari</option> 
                                                                        <option value="7">Pendidikan Tertiari Peringkat Pertama Di Tahap Sijil/Diploma</option> 
                                                                        <option value="8">Pendidikan Tertiari Peringkat Pertama Di Tahap Ijazah Sarjana</option> 
                                                                        <option value="9">Muda/Diploma Lanjutan/Sarjana</option> 
                                                                        <option value="10">Pendidikan Tertiari Peringkat Kedua</option> 
                                                                       
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-5">
                                                                    <label for="pwd">D19. Apakah sijil tertinggi yang anda perolehi?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d19">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Tiada</option>
                                                                        <option value="2">UPSR/ UPSRA atau yang setaraf</option> 
                                                                        <option value="3">PT3/ PMR/ SRP/ LCE/ SRA atau yang setaraf</option> 
                                                                        <option value="4">SPM atau yang setaraf</option> 
                                                                        <option value="5">STPM atau yang setaraf</option> 
                                                                        <option value="6">Sijil kemahiran khusus atau teknikal (TVET)</option>
                                                                        <option value="7">Sijil Politeknik/ Universiti/ Badan-badan yang memberi pengikhtirafan atau yang setaraf</option> 
                                                                        <option value="8">Diploma/ Diploma lanjutan dalam kemahiran khusus atau teknikal(TVET)</option> 
                                                                        <option value="9">Diploma Politeknik/ Universiti atau yang setaraf</option> 
                                                                        <option value="10">Ijazah/ Diploma lanjutan/ Sarjana</option> 
                                                                        <option value="11">Doktor falsafah (PHD)</option> 
                                                                    </select>
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D20. Dari manakah anda memperoleh sijil / diploma / ijazah tersebut?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d20">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Dalam Negara</option>
                                                                        <option value="2">Luar Negara</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D21. Bidang Pengajian Utama?</label><br><br>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d20">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Generic programmes and qualifications</option>
                                                                        <option value="2">Education</option>
                                                                        <option value="2">Arts and humanities</option> 
                                                                        <option value="2">Social sciences, journalism and information</option> 
                                                                        <option value="2">Business, administration and law</option> 
                                                                        <option value="2">Natural sciences, mathematics and statistics</option> 
                                                                        <option value="2">Information and communication technologies (ICTs)</option> 
                                                                        <option value="2">Engineering, manufacturing and construction</option> 
                                                                        <option value="2">Agriculture, forestry, fisheries and veterinary</option> 
                                                                        <option value="2">Health and welfare</option> 
                                                                        <option value="2">Services</option>  
                                                                    </select>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d21">
                                                                </div>
                                                                </div>
                                                                <label for="pwd"><br><br>Ekonomi</label><br>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D22. Adakah anda bekerja sekurang-kurangnya satu (1) jam dalam tujuh (7) hari yang lalu?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d17b">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya</option>
                                                                        <option value="0">Tidak</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D23. Adakah anda mempunyai apa-apa kerja yang akan dikerjakan kembali?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d23">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya</option>
                                                                        <option value="0">Tidak</option> 
                                                                    </select>
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D24. Adakah anda mencari kerja dalam masa tujuh (7) hari yang lalu?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d24">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Ya</option>
                                                                        <option value="0">Tidak</option> 
                                                                    </select>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D25. Apakah sebab utama anda tidak mencari pekerjaan?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d20">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Bersekolah/ masih belajar/ program latihan</option>
                                                                        <option value="2">Kerja rumah/ tanggungjawab keluarga/ komuniti</option>
                                                                        <option value="2">Percaya tiada kerja/ tiada kerja yang sesuai</option> 
                                                                        <option value="2">Keadaan cuaca</option> 
                                                                        <option value="2">Sakit/ bersalin atau keguguran</option> 
                                                                        <option value="2">Akan memulai menjawat kerja baru</option> 
                                                                        <option value="2">Akan melanjutkan pelajaran> 
                                                                        <option value="2">Hilang upaya/ keilatan</option> 
                                                                        <option value="2">Tiada minat/ baru tamat belajar</option> 
                                                                        <option value="2">Menunggu keputusan permohonan pekerjaan/ telah mencari kerja terdahulu dari minggu lalu</option> 
                                                                        <option value="2">Tiada kelayakan/ kurang kemahiran</option>
                                                                        <option value="2">Sudah bersara/ lanjut usia</option>  
                                                                    </select>
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                
                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd">D26. Pekerjaan<br>a)Apakah pekerjaan anda?</label><br><br>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d27a">
                                                                </div>
                                                                <div class="form-group col-md-4"><br>
                                                                    <label for="pwd">b)Terangkan tugas / jenis kerja yang dilakukan?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d27b">
                                                                </div>
                                                                </div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label for="pwd">D27. Apakah aktiviti / perkhidmatan / pengeluaran tempat kerja anda?</label>
                                                                    <input type="text" class="form-control" id="usr" value="" name = "bair_d128">
                                                                </div>

                                                                <div class="form-group col-md-4">
                                                                    <label for="pwd">D28. Taraf pekerjaan</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d26">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Majikan</option>
                                                                        <option value="2">Pekerja Kerajaan</option> 
                                                                        <option value="3">Pekerja Swasta</option> 
                                                                        <option value="4">TiBekerja Sendiri</option> 
                                                                        <option value="5">Pekerja keluarga tanpa gaji</option> 
                
                
                                                                    </select>
                                                                </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="form-group col-md-6">
                                                                    <label for="pwd">D29. Jenis pengangkutan ke tempat kerja?</label>
                                                                    <select type="text" class="form-control" id="usr" name="bair_d29">
                                                                        <option value="" selected>Sila pilih..</option>
                                                                        <option value="1">Awam</option>
                                                                        <option value="2">Disediakan oleh majikan</option> 
                                                                        <option value="3">Persendirian</option> 
                                                                        <option value="4">Tidak Berkaitan (cth:jalan kaki/bekerja di rumah)</option> 
                                                                        <option value="5">Lain-lain (cth:carpool/pickup van atau lori)</option> 
                                                                    </select>
                                                                </div>
                                                                </div>
                                                            </form>
                                                                    
                                                    </div>
                                                    </div>
                                        
                                                    <div class="col-sm-6 col-md-2">
                                                        <br><br>
                                                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                                                            <a href="">
                                                            <button class="btn btn-block btn-success" style="background-color:green" type="button"><i class="fa fa-edit"></i> Kemaskini</button>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        <div class="col-6 col-sm-4 col-md-2 col-xl mb-3 mb-xl-0">
                                                            <a href="">
                                                                <button class="btn btn-block btn-success"style="background-color:green" type="button"><i class="fa fa-check"></i> Sah</button>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        
                                                    </div>
                                                    
                                                  
                                </div>
                            
                        </div>
                    </div>
                  
                </div>
            </div>

        </div>
    </div>
    <!-- /.row-->
</div>
@endsection