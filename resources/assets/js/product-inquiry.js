(function ($) {
  let suppliers = []

  $(document).on('open', '.hider', function (e, params) {
    // params.companyId will get you the vendor's ID.
    // Here you can call your ajax API to update the counter.
  })

  $(document).on('click', '[data-toggle="inquire-button"]', function (e) {
    // Check if there are vendor selected.
    let checkedInputs = $('[data-toggle="inquiry-module"] input[type="checkbox"]:checked')

    suppliers = []

    if (!checkedInputs.length) {
      e.preventDefault()

      alert('Please select at least one supplier to start the inquiry.')
    } else {
      let checkedCount = checkedInputs.length
      let productInquiryModule = $('[data-toggle="inquiry-module"]').get(0)

      if (productInquiryModule) {
        let productName = $(productInquiryModule).data('product')
        let inquiryForm = $('#modal-inquiry form:first').get(0)

        // Update the supplier count.
        $('#modal-inquiry [data-supplier-count]').html(checkedCount)

        $('#modal-inquiry .product-name').html(productName)

        // Clear the body table.
        $('#modal-inquiry .table tbody').empty()

        $(checkedInputs).each(function () {
          let companyBlock = $(this).closest('[data-company]').get(0)

          if (companyBlock) {
            let supplierName = $(companyBlock).find('[data-name]').data('name')
            let supplierLocation = $(companyBlock).find('[data-location]').data('location')
            let supplierId = $(this).val()
            let row = $('<tr></tr>')
            let nameCell = $('<td></td>').html(supplierName)
            let locationCell = $('<td></td>').html(supplierLocation)
            row.append(nameCell).append(locationCell)

            $('#modal-inquiry .table tbody').append(row)

            suppliers.push({
              id: parseInt(supplierId),
              name: supplierName,
              location: supplierLocation
            })
          }
        })

        // Insert the supplier's id into the hidden input field.
        if (inquiryForm) {
          // Remove the existing hidden field first.
          $(inquiryForm).find('input.suppliers[type="hidden"]').remove()

          for (let supplier of suppliers) {
            let hiddenInput = $('<input/>').attr({
              type: 'hidden',
              value: supplier.id,
              name: 'suppliers[]',
              class: 'suppliers'
            })

            $(inquiryForm).append(hiddenInput)
          }
        }

        // Show the modal.
        $('#modal-inquiry').modal('show')
      }
    }
  })

  $(document).on('click', '[data-toggle="submit-inquiry"]', function (e) {
    /* Here you can do the ajax submission to server. */
    let message = $('#modal-inquiry textarea.form-control').val()
    let productId = $('[data-toggle="inquiry-module"]').data('product-id')
    console.log(message)
    console.log(productId)
    console.log(suppliers)
  })
})(jquery)
