(function ($) {
  $('[data-toggle="preview"]').each(function () {
    let thumbContainer = $(this).find('.preview-thumbnail')
    let previewContainer = $(this).find('.preview-pic')

    if (thumbContainer.length && previewContainer.length) {
      thumbContainer = thumbContainer.get(0)
      previewContainer = previewContainer.get(0)

      $(thumbContainer).on('click', 'li a.link', function(e) {
        e.preventDefault()

        // Find out the index of this list item in the thumbnail list.
        let li = $(this).parent().get(0)

        if (li) {
          let index = $(li).index()

          // Remove any active thumbnail class.
          $(thumbContainer).find('li.active').removeClass('active')
          $(li).addClass('active')

          // Check if the full image of that index exists.
          let activeTabPane = $(previewContainer).find('.tab-pane:eq(' + index + ')').get(0)

          if (activeTabPane) {
            // Now let's hide the main picture and put active class name to the specific image.
            $(previewContainer).find('.tab-pane.active').removeClass('active')
            $(activeTabPane).addClass('active')
          }
        }
      })
    }
  })
})(jquery)
