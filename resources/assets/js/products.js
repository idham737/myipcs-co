import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxiosPlugin from 'vue-axios-plugin'
// Import the pages here.
import ProductPage from './pages/products'

Vue.use(VueRouter)
Vue.use(VueAxiosPlugin, {
  reqHandleFunc: (config) => {
    let meta = $('meta[name="csrf-token"]').get(0)

    if (meta) {
      let token = $(meta).attr('content')

      // Append the token into the header.
      config.headers['X-CSRF-TOKEN'] = token
    }

    return config
  }
})

const routes = [
  {
    path: '/products',
    name: 'products',
    component: ProductPage
  },
  {
    path: '/products/category/:name',
    component: ProductPage,
    name: 'product-category',
    props: (route) => ({
      category: route.params.name
    })
  }
]

Vue.filter('lowercase', function(str) {
  str = String(str)

  return str.toLowerCase()
})

Vue.filter('thousands', function (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
})

const router = new VueRouter({
  routes,
  mode: 'history'
})

const app = new Vue({
  el: '#products-app',
  router,
  template: `<router-view />`
})
