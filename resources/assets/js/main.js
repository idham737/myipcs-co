(function ($) {
  /**
   * This function used to toggle between two modal.
   * The first parameter is the target it should show
   * and the second one is the one to close.
   */
  window.toggleModal = function (target, close) {
    $('#' + close).modal('hide').one('hidden.bs.modal', function () {
      // Remove the backdrop.
      $('body').removeClass('modal-open')
      $('.modal-backdrop').remove()

      setTimeout(function () {
        $('#' + target).modal('show')
      }, 1)
    })
  }

  // Use this to prevent the dropdown menu being closed on clicked.
  $(document).on('click', '[data-prevent-close]', function (e) {
    e.stopPropagation()
  })

  $('[data-readmore]').each(function () {
    let settings = {}

    for (let name in $(this).data()) {
      let value = $(this).data(name)

      if (value) {
        settings[name] = value
      }
    }

    $(this).readmore(settings)
  })

  $(document).scroll(function () {
    let scrollTop = $('.scroll-top').get(0)
    let position = $(this).scrollTop()

    if (scrollTop) {
      let isActive = $(scrollTop).hasClass('active')

      if (!isActive && position > 0) {
        $(scrollTop).addClass('active')
      } else if (isActive && position == 0) {
        $(scrollTop).removeClass('active')
      }
    }
  })

  $(document).on('click', '.scroll-top button', function (e) {
    e.preventDefault()

    let body = $('html, body')
    body.stop().animate({scrollTop: 0}, 500, 'swing')
  })

  // Initialize checkall buttons.
  $('[data-toggle="checkall"]').on('click', function () {
    let selector = $(this).data('selector')
    let currentState = $(this).data('checkall-state')
    let selectText = $(this).data('select-text')
    let deselectText = $(this).data('deselect-text')

    if (!currentState) {
      currentState = 'off'

      $(this).data('checkall-state', 'off')
    }

    let nextState = currentState === 'off' ? 'on' : 'off'

    let checkboxes = $(selector)

    if (checkboxes.length) {
      $(checkboxes).each(function () {
        if ($(this).is(':checkbox')) {
          if (nextState === 'on') {
            $(this).prop('checked', true)
          } else {
            $(this).prop('checked', false)
          }
        }
      })

      $(this).data('checkall-state', nextState)
      if (nextState === 'on') {
        $(this).html(deselectText)
      } else {
        $(this).html(selectText)
      }
    }
  })

  // Allow any element to open a tab when clicked.
  $(document).on('click', '[data-open-tab]', function (e) {
    e.preventDefault()

    let string = $(this).data('openTab')
    let delay = $(this).data('delay')
    let elementIds = string.split(/\,/)

    if (elementIds && elementIds.length) {
      for (let id of elementIds) {
        id = String(id).trim()
        let element = $(id).get(0)

        if (element) {
          if (delay) {
            setTimeout(() => {
              $(element).tab('show')
            }, delay)
          } else {
            $(element).tab('show')
          }
        }
      }
    }
  })

  // Initialize hider.
  $(document).on('click', '.hider', function (e) {
    if ($(this).hasClass('unhide')) {
      return
    }

    e.preventDefault()

    let element = this

    let triggerEvent = function () {
      let elementData = $(element).data()
      let params = {}

      if (Object.keys(elementData).length) {
        for (let name in elementData) {
          if (name.match(/^param[A-Z]\S+$/)) {
            // Remove the param and make the first character to lower case.
            let paramName = name.substr(5,1).toLowerCase() + name.substr(6)
            let value = elementData[name]

            params[paramName] = value
          }
        }
      }

      if (Object.keys(params)) {
        $(element).trigger('open', [params])
      } else {
        // No parameter. Just trigger the event without parameter.
        $(element).trigger('open')
      }
    }

    $(this).addClass('unhide')

    // Mark this hider.
    let key = $(this).data('key')

    if (key) {
      let localStorage = window.localStorage
      let hiderItems = localStorage.getItem('hider_items')
      let items = []

      if (hiderItems) {
        try {
          items = JSON.parse(hiderItems)
        } catch (ex) {
          items = []
        }
      }

      if ($.inArray(key, items)) {
        items.push(key)

        // Trigger event.
        triggerEvent()
      }

      // Store back to the localStorage.
      localStorage.setItem('hider_items', JSON.stringify(items))
    } else {
      triggerEvent()
    }
  })

  // Check any hider that has already entered previously.
  $('.hider').each(function () {
    let key = $(this).data('key')

    if (key) {
      let localStorage = window.localStorage

      let items = localStorage.getItem('hider_items')

      if (items) {
        try {
          items = JSON.parse(items)

          if ($.inArray(key, items) !== -1) {
            $(this).addClass('unhide')
          }
        } catch (ex) {
          // Repair the json string.
          items = []

          localStorage.setItem('hider_items', JSON.stringify(items))
        }
      }
    }
  })

  // Initialize any form with validation.
  $(document).on('submit', 'form.needs-validation', function (e) {
    let errorFields = $(this).find('.is-invalid')

    if (false === this.checkValidity() || errorFields.length) {
      e.preventDefault()
      e.stopPropagation()
    } else {
      if ($(this).data('prevent-resubmit')) {
        // Find the submit button and disable it.
        $(this).find('button[type="submit"], input[type="submit"]').prop('disabled', true)
      }
    }

    $(this).addClass('was-validated')
  })

  // Initialize the collapse toggle.
  $(document).on('show.bs.collapse', '.collapse', function (e) {
    let id = $(this).attr('id')
    let target = $('[href="#' + id + '"]')

    if (target.length) {
      if ($(target).hasClass('accordion-toggle')) {
        $(target).addClass('active')
      }
    }
  })
  $(document).on('hide.bs.collapse', '.collapse', function (e) {
    let id = $(this).attr('id')
    let target = $('[href="#' + id + '"]')

    if (target.length) {
      if ($(target).hasClass('accordion-toggle')) {
        $(target).removeClass('active')
      }
    }
  })

  $(document).on('keyup', 'form.needs-validation .is-invalid', function (e) {
    $(this).removeClass('is-invalid')
  })

  // Email verification.
  $(document).on('blur', '[data-email-verification]', function (e) {
    let value = $(this).val()
    let formGroup = $(this).closest('.form-group').get(0)

    if (value && formGroup) {
      $(this).prop('disabled', true)

      let invalidFeedback = $(formGroup).find('.invalid-feedback').get(0)

      if (invalidFeedback) {
        let invalidFeedbackText = $(invalidFeedback).html()

        if (!$(this).data('default-invalid-feedback')) {
          $(this).data('default-invalid-feedback', invalidFeedbackText)
        } else {
          invalidFeedbackText = $(this).data('default-invalid-feedback')
        }

        // Prevent the form being submit.
        $(this).closest('form').find('button[type="submit"], input[type="submit"]').prop('disabled', true)

        $.ajax({
          type: 'get',
          url: '/api/register/email-check',
          dataType: 'json',
          data: {
            email: value
          },
          context: this,
          success: function (data) {
            if (!data || !data.success) {
              $(invalidFeedback).html('The email has already been taken.')
              $(this).addClass('is-invalid')
            } else {
              $(invalidFeedback).html(invalidFeedbackText)
              $(this).removeClass('is-invalid')
            }

            $(this).prop('disabled', false)

            $(this).closest('form').find('button[type="submit"], input[type="submit"]').prop('disabled', false)
          },
          error: function () {
            $(this).prop('disabled', false)

            $(this).closest('form').find('button[type="submit"], input[type="submit"]').prop('disabled', false)
          }
        })
      }
    }
  })

  $(document).on('keyup', '[data-reset-oninput]', function (e) {
    let form = $(this).closest('form')

    if (form.length) {
      form = $(form).get(0)

      let invalidFields = $(form).find('.is-invalid')

      if (invalidFields.length) {
        $(invalidFields).removeClass('is-invalid')
      }
    }
  })

  let moveToHashAnchor = function (hashId) {
    let anchorElement = $('#' + hashId).get(0)

    if (anchorElement) {
      let offset = $(anchorElement).offset()

      $('html, body').stop().animate({
        scrollTop: offset.top
      }, 500, 'swing')
    }
  }

  $(document).on('click', 'a[href*="#"]', function (e) {
    // e.preventDefault()
    if ($(this).data('toggle') !== 'tab') {
      let hash = $(this).attr('href')
      let position = hash.indexOf('#')

      if (position !== -1) {
        hash = hash.substr(position + 1)

        if (hash) {
          moveToHashAnchor(hash)
        }
      }
    }
  })

  $('[data-toggle="tooltip"]').tooltip()

  // Social Shares
  $(document).on('click', '[data-toggle="social-share"]', function (e) {
    e.preventDefault()

    let url = $(this).data('url')
    let type =$(this).data('type')

    if (type === 'facebook') {
      window.open("https://www.facebook.com/sharer/sharer.php?u=" + url, "pop", "width=600, height=400, scrollbars=no")
    } else if (type === 'twitter') {
      window.open("http://twitter.com/share?url=" + url, "pop", "width=600, height=400, scrollbars=no")
    } else if (type === 'google') {
      window.open("https://plus.google.com/share?url=" + url, "pop", "width=600, height=400, scrollbars=no")
    } else if (type === 'instagram') {

    }
  })

  let hashValue = window.location.hash

  if (hashValue) {
    let hashId = hashValue.substring(1)

    moveToHashAnchor(hashId)
  }

  // Check opentab query parameter.
  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  let openTab = getParameterByName('opentab')

  if (openTab) {
    let tabElement = $('#' + openTab).get(0)

    if (tabElement) {
      $(tabElement).tab('show')
    }
  }
})(jquery)
